#1.linux 体系结构
    用户态 + 内核态
    
#1.shell 命令解释器
    
#1.查找特定的文件
    find ：在指定目录下查找文件
        支持通配符
        
        从当前目录查找
            find -name 'haha.java'
        
        从根目录查找
            find / -name "haha.java"
            
        -iname : 忽略大小写
#1.查找文件内容
    grep
        查找文件里符合规则的字符串
        
        从target开头文件里 找到 haha 字符串的行
        grep "haha" target*
        
    |:管道操作符
        前一个指令 作为输入 给后一个指令使用
        
    
   
#1.对日志内容做统计
   
    awk：一次读取一行文本，按输入分隔符进行分片
        擅长对列进行操作
    将切片保存在内置的 $1 $2
    
    awk '{pring $1 $4}' netstat.txt
    
    awk -F ","  '{print $2}' test.txt   
    
    awk '$1=="tcp" $2="1" {print $2}'
    
    
#1.批量替换文档的内容
    sed指令 stream editor 流编辑器
        擅长对行进行操作
        
        sed 's/^Str/String/g' replace.java
        sed 's/\.$/\;/' replace.java
        sed -i 's#\r##g' install.sh
            -i  将修改保存到文件
        三个反斜杠
         s字母表示 进行字符串的操作
        第一个： 以Str开头的
        第2个： 替换成String
        第三个：加g 全部替换
        
    可以用来删除某行
        sed -i '/Integer/d'
            