#1.缓存穿透
    缓存没有，查询到数据库
#1.熔断，直接返回
#1.缓存中间件memcache 和 redis
    memcache
        代码类似于hash
        不支持数据持久化
        不支持分片
        不支持主从
    redis
        数据类型丰富
        支持主从
        支持分片
        支持持久化
#1.mysql有自己缓存，为何还要使用redis
    速度：
        redis更快
        mysql缓存慢
    内存：
        吃jvm内存
        redis是分布式缓存
    分布式：
        缓存不会同步
        memcache redis 是分布式的缓存 属于远程调用另外的webserver
#1. 为什么redis这么快
    10万+ QPS query per second
        基于内存
        数据结构简单
        单线程，避免频繁创建销毁线程。主线程是单线程的。避免上下文切换，和锁竞争。
        多路io复用模型。
        

#1.多路io复用模型
    fd：文件描述符
    阻塞io模型
    io多路复用
        select系统调用
    
    react设计模式监听io事件
#1.redis数据类型
    string 
        二进制安全
    hash
        适合存放对象
    list
        类似于stack结构
    set
        string元素无序集合，不允许重复值。
        无序的
    sorted set
        有序的
        通过score 分数，对集合进行排序
    用于计数的hyperloglog
    
    
#1.从海量key中查询出某一个固定的前缀的key
    keys指令
        一次返回所有数据，数据量过大会卡住。
    scan指令
        每次返回少量数据
        基于游标的迭代器
#1.实现分布式锁
    实现过程：
        setnx指令
            set if not exist
            成功返回1 失败返回0
            
    解决长期有效问题
        expire 设置过期时间
        set指令 将setnx和expire柔和在一起
        
    大量key同一时间过期
        redis大量删除key，redis可能会卡顿
        在过期时间是加一个随机值
    互斥
    安全
    死锁
    容错
    
#1.实现异步队列
    第一种：
        rpush
            生产消息
            往右侧添加
        lpop
            消费消息
            从左侧弹出
            
            lpop不会等待，可以在应用层sleep逻辑。
        
    第二种：
        blpop：
            如果没有数据，会阻塞
            
    pub、sub模式
        一个生产者，多个消费者
        
#1.持久化方式RDB
    快照全量
    手动触发RDB
        方式一：
            save
                阻塞redis进程，知道rdb创建完毕。
        方式二：
            bgsave
                fork一个子进程来创建rdb文件，不阻塞服务器进程
                
    自动触发RDB
        在redis.conf    save
        shutdown如果没开启aof
        
        
#1.AOF append only file
    增量保存，默认是关闭的
    
    随着操作不断增加，aof文件不断增大，在redis不中断情况下，可以重构aof文件。
    
    RDB 和 AOF 优缺点
        全量数据快照，文件小，恢复快
        缺点：无法保存最后一次快照之后的数据。
        
        aof：可读性高，适合保存增量数据
        文件打，恢复慢。
        
    RDB AOF混合持久化方式
        先以RDB方式写入全量数据，然后使用AOF追加方法
        使用bgsave做镜像全量持久化，aof做增量持久化
#1.pipeline及主从同步
    和linux的pipeline类似
    redis基于请求响应模式，单个请求需要一一应答。
    pipeline批量执行指令，节省多次io往返的时间。
        指令直接不可以有依赖。
        
    redis同步机制：
        
    主从同步原理：
        一个Master，多个slave。
            master写操作
            slave读操作
        
        弱一致性：
            不一定实时同步
        最终一致性：
            最终是一致的。
    
    全同步过程：
        slave发送sync到master
        master启动一个后台进程，将redis内容保存到文件中。
        master将保存快照期间内存缓存起来，一起发送给slave
        使用新的RDB文件替换旧的RDB文件
        
    增量同步：
        master接受到操作指令，判断是否需要扩散给slave
        将操作记录追加到aof中
        将操作扩展到其他的slave
        将缓存中数据发送给slave
        
     弊端：
        不具有高可用性，master挂掉之后，无法提供服务
        
        解决方法：
            哨兵机制：
     
     解决主从同步过程中master宕机，主从切换问题
        监控主服务器是否正常
        通过api向管理员发送故障通知
        自动故障迁移：主从切换
        
     
     流言协议Gossip
        在杂乱无章中寻求一致
        
#1.redis 集群
    如何从海量数据中快速找到所需
        分片：按照某种规则划分数据，放到不同的节点上。
        
    一致性哈希算法：
        对2^32次取模
        2^32形成一个圆环。
        
        hash环数据倾斜问题
            引入虚拟节点
#1.