#1.谈谈你对java的理解
    问题很宽泛
    
    平台无关性
        不同平台有不同的jvm实现。
        
    垃圾回收机制
    语言特性
        泛型，反射，lambda
    面向对象
    类库
    异常处理
    
#1.jvm如何加载class文件
    jvm 是一种抽象的计算机，内存中的虚拟机
        有自己的寄存器
        指令系统
        堆栈
        处理器
    
    jvm
        class loader
            加载class文件到内存
        runtime area
            stack
            heap
            方法区
            程序计数器
            本地方法区
        execution engine
            对命令进行解析
        native interface
            融合不同编程语言为java所用
    
     class loader
           
#1.什么是反射
    java反射机制是在运行状态中，对于任意一个类，都能知道他的属性和方法，对于任意一个对象，都可以调用他的方法和属性，
    这种动态获取信息，以及动态调用方法功能称为java的反射
    bootstrap
    反射就是把类中的各个属性，方法，映射成一个个的对象。通过对象来获取属性的值，以及方法的调用。
#1.谈谈classloader
    classloader将class文件，加载成jvm中Class对象
    利用Class对象，创建多个实例对象。
    
    种类
        bootstrap 加载器
            加载核心库 java
        ExtClassLoader
            java编写，加载扩展库 java
        AppClassLoader
            加载classpath下的 java
            
        自定义ClassLoader
    
    
    双亲委派机制：
        当一个Hello.class这样的文件要被加载时。不考虑我们自定义类加载器，首先会在AppClassLoader中检查是否加载过，如果有那就无需再加载了。
        如果没有，那么会拿到父加载器，然后调用父加载器的loadClass方法。父类中同理也会先检查自己是否已经加载过，如果没有再往上。
        注意这个类似递归的过程，直到到达Bootstrap classLoader之前，都是在检查是否加载过，并不会选择自己去加载。
        
        直到BootstrapClassLoader，已经没有父加载器了，这时候开始考虑自己是否能加载了，如果自己无法加载，会下沉到子加载器去加载，
        一直到最底层，如果没有任何加载器能加载，就会抛出ClassNotFoundException。
#1.jvm内存结构模型
    线程私有：
        虚拟机栈
            每个方法创建一个栈帧
                用于存储局部变量表，操作栈，动态连接，返回地址
        本地方法栈
            为本地native方法服务
        程序计数器
            记录的是字节码指令地址
            逻辑计数器
            不会发生内存泄漏
    线程共有：
        堆：
            gc堆
            
            分代算法：
                新生代
                    eden
                    from
                    to 
                老年代
        方法区、
            永久代，元空间 是实现方法区
                元空间使用的本地内存
            类信息
            静态变量
            常量
        
#1.三大调优参数
    -Xms
        初始java堆大小
    -Xmx
        java堆最大值
    -Xss 
        虚拟机栈大小 256k
#1.垃圾回收之标记算法
    判断对象是否垃圾
        引用计数法
        可达性分析
            判断引用链是否可达
            从GCROOT开始
                栈中的变量

#1.垃圾回收算法
    标记-清除算法
        标记 - 直接清除
        
    复制算法
        适用于年轻代
    标记整理算法
        标记 - 移动所有对象
        
    分代收集算法
        年轻代
            复制算法
            minor gc
                每经过一次minorgc 对象加一岁，15岁以后进入到老年代。
            
            eden  8 
            from survivor 1
            to survivor 1
            
        老年代
            标记 - 整理
            full gc  major gc
            
            大对象直接进入老年代
#1.垃圾收集器
    stop the world
    
    safepoint
        快照点
        方法调用，循环跳转，异常跳转等都可以做安全点。
        
#1.常见的垃圾收集器
    serial 单线程
    parnew
    
    
    老年代收集器
        cms
        serial old
#1.
#1.