#1.概述
    abstract class AbstractList<E> extends AbstractCollection<E> implements List<E>

    基于随机访问 random access 的
    按照顺序访问，使用AbstractSequentialList接口。
    
    实现不可修改的list
        实现size方法
        实现get方法
    实现一个可修改的list
        实现size方法
        实现get方法
        
        实现set方法         
    实现一个可修改，长度可变的list
        实现size方法
        实现get方法
        实现set方法 
            
        实现add方法
        实现remove方法
    
    本类实现类iterator，listIterator，不需要用户实现了。
    他的迭代器的实现依赖set、remove方法实现。   
    
    sublist返回的这个list指向的是同一块数据区，相当于控制了区间的一个引用。 
#2.内部类
    Itr implements Iterator<E>
        int cursor = 0;
        int lastRet = -1;
            remove 一次 lastRet 就会置为 -1，只有下次调用 next方法时候才能将lastRet置为 cursor
        int expectedModCount = modCount;
            remove之前会检查这两个值是否相等，如果不相等，抛出异常。
            
    class ListItr extends Itr implements ListIterator<E>    
        set
            AbstractList.this.set(lastRet, e);
        add
            AbstractList.this.add(cursor, e);
            
    class SubList<E> extends AbstractList<E> 
        返回一个AbstractList的子类，依靠AbstractList子类来实现，
        
#3.需要实现的方法
        实现size方法
        实现get方法
        实现set方法 
        实现add方法
        实现remove方法
#4.增删改查
    新增
        public boolean add(E e)
            这个方法，直接抛出异常
        public void add(int index, E element)
        
        boolean addAll(Collection<? extends E> c);
            批量添加
    修改
        public E set(int index, E element)
    查询,遍历
        public boolean isEmpty()
            查询是否为空
        boolean contains(Object o);
            单个查询是否包含
        boolean containsAll(Collection<?> c);
            批量查询是否全部包含
    删除
        boolean remove(Object o);
            单个删除
        public E remove(int index)
        
        boolean removeAll(Collection<?> c);
            批量删除
        void clear();
            清空容器，删除所有

#3.其他方法
    ListIterator<E> listIterator()
    ListIterator<E> listIterator(final int index)
    List<E> subList(int fromIndex, int toIndex）
        sublist返回的这个list跟之前控制的同一元素，相当于控制了区间的一个引用。
    public int lastIndexOf(Object o)
#4.问题
