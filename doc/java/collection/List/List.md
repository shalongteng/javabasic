#1.概述List<E> extends Collection<E>
    相对于collection来说增加了
        操作下标的
            E get(int index);
            E set(int index, E element);
            void add(int index, E element);
            E remove(int index);
            boolean addAll(int index, Collection<? extends E> c);
            ListIterator<E> listIterator(int index);
            List<E> subList(int fromIndex, int toIndex);
        把java8之前工具类的方法，加入进来
            default void sort(Comparator<? super E> c)
                排序
            default void replaceAll(UnaryOperator<E> operator) 
                遍历list，对每个元素执行function
#2.增删改查
    新增              
        单个添加
            boolean add(E e);
            void add(int index, E element);
            
        批量添加
            boolean addAll(Collection<? extends E> c);
            boolean addAll(int index, Collection<? extends E> c);
    修改
        E set(int index, E element);
        
    查询,遍历
        E get(int index);
            根据下标查询
        boolean contains(Object o);
            单个查询是否包含
        boolean containsAll(Collection<?> c);
            批量查询是否全部包含
            
        返回改集合迭代器
            Iterator<E> iterator();
            ListIterator<E> listIterator();
            ListIterator<E> listIterator(int index);
    删除
        单个删除
            boolean remove(Object o);
            E remove(int index);
            
        批量删除
            boolean removeAll(Collection<?> c);
            
        void clear();
            清空容器，删除所有
        default boolean removeIf(Predicate<? super E> filter)
            根据条件删除
#3.jdk8新加入的方法
    default Spliterator<E> spliterator()
    default void sort(Comparator<? super E> c)
        排序
    default void replaceAll(UnaryOperator<E> operator) 
        遍历list，对每个元素执行function
#4.其他方法
    List<E> subList(int fromIndex, int toIndex);
        返回区间的list
    返回元素下标
        int indexOf(Object o);
        int lastIndexOf(Object o);
    判断相等
        boolean equals(Object o);
    int size();
        集合包含元素个数
    boolean isEmpty();
        集合是否为空
    转成数组
        Object[] toArray();
        <T> T[] toArray(T[] a);
        
    求交集
        boolean retainAll(Collection<?> c);
#4.常见的问题