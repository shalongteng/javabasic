#1.概述
    abstract class AbstractSequentialList<E> extends AbstractList<E>

    他是基于随机访问 random access 的，
    如果是按照顺序访问，使用AbstractSequentialList接口。
    
    实现不可修改的list
        实现listIterator 
            hasNext
            next
            hasPrevious
            previous
            nextIndex
        实现size
        
    实现一个可修改的list
        实现listIterator
            需要set
        实现size
        
        实现set方法         
    实现一个可修改，长度可变的list
        实现listIterator
            需要set
            需要remove
        实现size
    
    这个类set，add，remove等方法依赖于listIterator的实现。
#2.增删改查
    新增
        void add(int index, E element)
        boolean addAll(int index, Collection<? extends E> c)
            批量添加
    修改
        E set(int index, E element) 
    查询,遍历
        E get(int index)
    删除
        E remove(int index)
            单个删除

#3.其他方法
    Iterator<E> iterator()
    bstract ListIterator<E> listIterator(int index)
        唯一的抽象方法
#4.问题
