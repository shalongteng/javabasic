#1.arraylist概述
    存储
        Object[] elementData
        
    可以存储null元素
    
    修改结构性的方法：需要在外部加同步锁
        adds
        deletes
        扩容方法 grow
        
        set不属于
#2.字段
    modCount = 0;
        修改次数，list的添加 删除修改结构性操作都需要对modCount加一
        
    serialVersionUID
        在进行反序列化时，JVM会把传来的字节流中的serialVersionUID于本地相应实体类的serialVersionUID进行比较。
        如果相同说明是一致的，可以进行反序列化，否则会出现反序列化版本一致的异常，即是InvalidCastException。
        也就是说一致的话才认为这个对象是这个类的对象。
    int DEFAULT_CAPACITY = 10;
        默认容量是10
        capacity就是数组长度。
        
    Object[] elementData;
        存储数据的数组
#3.内部类
    迭代器：快速失败
        Itr implements Iterator<E>
            相对于abstractlist来说，多加了点校验
            forEachRemaining相对于Iterator里的实现来说，加了许多校验。
        ListItr extends Itr implements ListIterator<E>
    
    SubList extends AbstractList<E> implements RandomAccess
        返回的是list的一个视图view。
        list某个区间的视图
        
     ArrayListSpliterator<E> implements Spliterator<E>
        trySplit：返回的是左半边的Spliterator，原来的Spliterator变成了右半边。
            
#4.构造器
    public ArrayList(int initialCapacity)
        如果initialCapacity等于0，那么capacity就是10
    public ArrayList()
        this.elementData = DEFAULTCAPACITY_EMPTY_ELEMENTDATA;
        默认在add元素时候，会把capacity修改成10
        
    public ArrayList(Collection<? extends E> c)
#5.增删改查
    增加：
        add(E e)
        add(int index, E element)
        boolean addAll(Collection<? extends E> c)
        boolean addAll(int index, Collection<? extends E> c)
        
    删除： 
        单个删除
            E remove(int index)
            boolean remove(Object o)
        批量删除
            boolean removeAll(Collection<?> c)
            boolean removeIf(Predicate<? super E> filter)
                根据条件批量删除
                
            void clear()
                length是不变的。
    修改:
        E set(int index, E element)
#6.其他方法
    void replaceAll(UnaryOperator<E> operator)
        每个元素 进行一次map
    void sort(Comparator<? super E> c)
    boolean retainAll(Collection<?> c)
        求交集
#7.问题
    arraylist慢的因素：
        ArrayList扩容时候，需要数组拷贝。
        
    ArrayList里为何是object数组，不是泛型数组
        java不允许泛型数组
        
    1.7和1.8版本空构造器的区别
        1.7的时候是初始化就创建一个容量为10的数组，
        1.8后是初始化先创建一个空数组，第一次add时才扩容为10
        
    ArrayList线程不安全体现在哪里
        add，delete，grow 都会不安全
        https://my.oschina.net/u/3847203/blog/4529997
        其一：发生在赋值时候。后执行线程覆盖前面的线程。
            elementData[size++] = e;不是一个原子操作，是分两步执行的。
            elementData[size] = e; size++;
            
            列表为空 size = 0。
            线程 A 执行完 elementData[size] = e;之后挂起。A 把 "a" 放在了下标为 0 的位置。此时 size = 0。
            线程 B 执行 elementData[size] = e; 此时 size = 0，B 把 "b" 放在了下标为 0 的位置，把 A 的数据给覆盖掉了。
            线程 B 将 size 的值增加为 1。
            线程 A 将 size 的值增加为 2。
            
        其二：发生在扩容时候
        ArrayList 默认数组大小为 10。假设现在已经添加进去 9 个元素了，size = 9。
            线程 A 执行完 add 函数中的ensureCapacityInternal(size + 1)挂起了。
            线程 B 执行，不需要扩容。把 "b" 放在了下标为 9 的位置，且 size 自增 1。此时 size = 10。
            线程 A 执行，把 "a" 放在下标为 10 的位置，因为 size = 10。数组还没有扩容，最大的下标才为 9，
                所以会抛出数组越界异常 ArrayIndexOutOfBoundsException
    
    满了之后add
        内存溢出
            2147483647如果一个对象1kb，那么这就有上千GB,内存一般没有这么大。
            
        数组越界
            当size达到int最大值以后，size++ 这个运算并不会报错
            
        解决方法：
            换成Linkedlist，但是查询太慢
            map
            数据库等
        