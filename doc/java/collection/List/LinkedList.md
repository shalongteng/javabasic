#1.概述
    LinkedList<E>extends AbstractSequentialList<E> 
        implements List<E>, Deque<E>, Cloneable, Serializable
    
    这是一个双向链表的实现。可以存储null元素。
    
    修改结构化的操作
        add，delete
        set不属于

#2.常用属性
    int size = 0;
    Node first：头节点，也就是第一个节点。
    Node last：尾节点，也就是最后一个节点。
#3.内部类
    static class Node<E>
        E e;存储集合中的元素，每一个node节点存储一个元素。
        Node prev:当前节点的前一个节点的引用
        Node next：当前节点的后一个节点的引用
        
    class ListItr implements ListIterator<E>
    class LLSpliterator<E> implements Spliterator<E>

#4.辅助方法
    //向头部插入一个元素，把这个元素设置为头结点。
    private void linkFirst(E e) {
        //先把头结点用f保存一下
        final Node<E> f = first;
        //使用e创建一个节点。
        final Node<E> newNode = new Node<>(null, e, f);
        //将头结点指向新创建节点。
        first = newNode;
        //如果f==null 那么这就是插入的第一个元素
        if (f == null)
            //把last指向刚创建的节点
            last = newNode;
        else
            //否则就把f的前驱指向刚创建的节点
            f.prev = newNode;
        size++;
        modCount++;
    }
    void linkLast(E e) 
        和linkFirst原理类似
        
    //succ前面插入一个节点e
    void linkBefore(E e, Node<E> succ) {
        // assert succ != null;
        final Node<E> pred = succ.prev;
        //新创建的节点 的后置指针指向 succ
        final Node<E> newNode = new Node<>(pred, e, succ);
        // succ的前置指针指向 新创建的节点
        succ.prev = newNode;
        if (pred == null)
            //pred == null succ就是头节点 这时候把头节点置为新建的节点
            first = newNode;
        else
            pred.next = newNode;
        size++;
        modCount++;
    }
    
    
    private E unlinkFirst(Node<E> f) 
        解除头部元素f
    private E unlinkLast(Node<E> l)
        解除尾部元素l
        
    E unlink(Node<E> x)
        解除一个元素，可以在任何位置。
        
    node方法
        
#5.增删改查
    新增
    修改
    查询,遍历
    删除

#6.其他方法
#7.问题
