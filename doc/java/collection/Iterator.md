#1.概述
    迭代器模式
        每种容器遍历方式不同，为了使得容器遍历统一起来。每种容器返回一个标准的iterator。
        
    E next();
        返回当前元素，并且把游标往下移动一位。
#2.方法
    boolean hasNext();
        是否还有下一个元素
    E next();
        返回当前元素，并且把游标往下移动一位。
    default void remove() {
         throw new UnsupportedOperationException("remove");
    }
        remove()是唯一安全的方式来在迭代过程中修改集合；在迭代中修改了集合将会产生未知的行为。
        而且每调用一次next()方法，remove()方法只能被调用一次，如果违反这个规则将抛出一个异常。
    

#3.jdk8新加入的方法
    default void forEachRemaining(Consumer<? super E> action) {
            Objects.requireNonNull(action);
            while (hasNext())
                action.accept(next());
    }
        对每一个剩余的元素（当前游标后面所有的元素），进行一个消费动作。