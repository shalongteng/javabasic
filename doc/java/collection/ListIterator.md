#1.概述 
    ListIterator继承于Iterator接口,只能用于各种List类型的访问。
    
    可以通过调用listIterator()方法产生一个指向List开始处的ListIterator, 
    可以调用listIterator(n)方法创建一个cursor指向列表索引为n的元素处的ListIterator
    
    由以上定义我们可以推出ListIterator可以:
        (1)双向移动（向前/向后遍历）.
        (2)产生相对于迭代器在列表中指向的当前位置的前一个和后一个元素的索引.
        (3)可以使用set()方法替换它访问过的最后一个元素.
        (4)可以使用add()添加一个元素，添加的元素就在cursor的位置。
        
    游标的位置处于两个元素之间：
                               Element(0)   Element(1)   Element(2)   ... Element(n-1)
          cursor positions:  ^            ^            ^            ^                  ^
        
#2.方法
    向后遍历：
        boolean hasNext();
            是否还有下一个元素
        E next();
            返回当前元素，并且把游标往下移动一位。
    向前遍历：
        boolean hasPrevious();
        E previous();
    获取下标值 
        int nextIndex();
            next方法返回的元素的下标。
        int previousIndex();
            获取previous的下标，如果是最一开始，返回-1.

    修改list
        void remove();
        void set(E e);
        void add(E e);
#3.jdk8新加入的方法
    default void forEachRemaining(Consumer<? super E> action) {
            Objects.requireNonNull(action);
            while (hasNext())
                action.accept(next());
    }