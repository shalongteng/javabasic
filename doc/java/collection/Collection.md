#1.概述
    Collection 是一个接口，子接口有
        List
        Set
    Collection 是根接口，表示其他若干个接口的共性而出现的接口。
        collection中对象equals的使用
            remove方法
            contains方法
        collection中iterator的使用
            containsAll
            removeAll
    
#2.增删改查
    新增
        单个添加
            boolean add(E e);
        批量添加
            boolean addAll(Collection<? extends E> c);
    修改
        
    查询,遍历
        单个查询是否包含
            boolean contains(Object o);
        批量查询是否全部包含
            boolean containsAll(Collection<?> c);
        返回改集合迭代器
            Iterator<E> iterator();
    删除
        单个删除
            boolean remove(Object o);
                参数是Object
        批量删除
            boolean removeAll(Collection<?> c);
        清空容器，删除所有
            void clear();
        根据条件删除
            default boolean removeIf(Predicate<? super E> filter)
                1.8新加的方法
#3.流相关方法
    default Spliterator<E> spliterator() 
    default Stream<E> stream() 
    default Stream<E> parallelStream() 
    
#4.其他方法
    int size();
        集合包含元素个数
    boolean isEmpty();
        集合是否为空
    boolean retainAll(Collection<?> c);
        求交集
        
    集合转数组
        Object[] toArray();
            返回的是新开辟的空间，用来保存元素。
        <T> T[] toArray(T[] a);
#5.问题
    1.如何集合元素达到了int最大值，继续add会怎么样。
    
    2.为什么Collection接口的remove方法参数类型是Object而不是泛型E
        要么o和e都为空，要么o.equals(e)，
        这两个判断中都没有对o和e的类型做限制，o和e都为空很好理解，
        equal方法继判断两个对象是否相等没有类型限制，只要equal方法返回true成立，则删除容器元素。
