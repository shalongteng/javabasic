#1.概述
    interface Queue<E> extends Collection<E> 
    
    插入，取出，检查操作都有两种形式
        一中抛异常
        一中返回null或者false
    
    queue中最后不要插入null，因为poll在队列为空的时候，返回null。
#2.增删改查
    新增
        boolean add(E e)
            插入失败返回异常。如果没有空间就会插入失败。
        boolean offer(E e)
            插入失败返回false
            他用在插入失败属于正常的情况下，而不是异常，通常用在有界队列和固定容量的队列。
        
    查询：返回头元素
        E element()
            队列空了，抛出NoSuchElementException
        E peek()
            队列空了，返回null
        
    删除：删除的是头元素
        E remove()
            当队列空了，返回异常
        E poll()
            当队列空了，返回null。
            
#4.问题
