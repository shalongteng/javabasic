#1.概述
    interface Deque<E> extends Queue<E>
    
    deque：double ended queue
    
    双端队列
        从两端都可以进行插入，取出操作。
    双端队列分类    
        stack方法
        queue方法
        插入，取出，检查操作都有两种形式
            一中抛异常
            一中返回null或者false ： 一般用于容量有限制的队列中。
    
    最好不要插入null，因为poll在队列为空的时候，返回null。
    
    可以被用作队列 
        队列添加到尾部，删除的是头部。
    可以被用作栈。
        这个接口优先于stack类的使用。
    
    不抛出异常的有
        offer poll peek
        
#2.作为双端队列的增删改查
    新增
        加入到头部：
            void addFirst(E e);
                当容量满了，抛出IllegalStateException
            boolean offerFirst(E e);
                容量有限制的队列用这个比较好
                当容量满了，返回false
        加入到尾部：
            void addLast(E e);
                当容量满了，抛出IllegalStateException
            boolean offerLast(E e);
                容量有限制的队列用这个比较好
                当容量满了，返回false
            boolean add(E e)
                和addlast相等。
            boolean offer(E e)
                和offerLast相等
        
    查询：
        查询头部元素
            E getFirst();
                队列为空，抛出NoSuchElementException
            E peekFirst();
                队列为空，返回null
                
            E element()
                和getFirst相等
            E peek()
                和peekFirst相等
        查询尾部元素
            E getLast();
                队列为空，抛出NoSuchElementException
            E peekLast();
                队列为空，返回null
        
    删除：
        删除头部元素
            E removeFirst();
                队列为空，抛出NoSuchElementException
            E pollFirst();
                队列为空，返回null
            E remove()
                和removefirst相等
            E poll()
                和pollFirst相等
        删除尾部元素
            E removeFirst();
                 队列为空，抛出NoSuchElementException
            E pollLast();
                队列为空，返回null
        根据条件的删除：
            boolean removeFirstOccurrence(Object o);
            boolean removeLastOccurrence(Object o);
        
#2.作为栈的增删改查       
     void push(E e);
        如果满了，抛出IllegalStateException
     E pop();
        从头部弹出一个元素，队列空了抛出NoSuchElementException
     peek()
        查看头部的元素
#3.其他方法
    int size();
    Iterator<E> iterator()
        元素从头部到尾部迭代
    boolean contains(Object o);
    Iterator<E> descendingIterator();
        翻转的迭代器，从尾部迭代到头部
#4.问题
