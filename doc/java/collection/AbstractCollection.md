#1.概述
    abstract class AbstractCollection<E> implements Collection<E>

    实现AbstractCollection需要实现iterator和size方法。这个类里的默认实现，大多依托iterator来实现的。
        
#2.需要实现的方法
    public abstract Iterator<E> iterator();
    public abstract int size();
    public boolean add(E e)
#3.增删改查
    新增
        public boolean add(E e)
            这个方法，直接抛出异常
        boolean addAll(Collection<? extends E> c);
            批量添加
    修改
    查询,遍历
        public boolean isEmpty()
            查询是否为空
        boolean contains(Object o);
            单个查询是否包含
        boolean containsAll(Collection<?> c);
            批量查询是否全部包含
    删除
        boolean remove(Object o);
            单个删除，调用迭代器的删除来实现。
            
        boolean removeAll(Collection<?> c);
            批量删除
        void clear();
            清空容器，删除所有

#4.从collection继承的方法
    default Spliterator<E> spliterator() 
    default Stream<E> stream() 
    default Stream<E> parallelStream() 
    default boolean removeIf(Predicate<? super E> filter)
        根据条件删除

#5.其他方法
    boolean retainAll(Collection<?> c);
        求交集
    转数组
        public Object[] toArray() 
        public <T> T[] toArray(T[] a)
#6.问题
    1.如何集合元素达到了int最大值，继续add会怎么样
        如果数组长度过大，可能出现的两种错误
        OutOfMemoryError: Java heap space  堆区内存不足（这个可以通过设置JVM参数 -Xmx 来指定）。
        OutOfMemoryError: Requested array size exceeds VM limit  超过了JVM虚拟机的最大限制，
    2.为什么最大长度要减8 （int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;）
        数组作为一个对象，需要一定的内存存储对象头信息，对象头信息最大占用内存不可超过8字节。