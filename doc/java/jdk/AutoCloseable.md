#1.BaseStream概述
    在1.7之前，我们通过try{} finally{} 在finally中释放资源。
   
    在finally中关闭资源存在以下问题：
       (1)自己要手动写代码做关闭的逻辑；
       (2)有时候还会忘记关闭一些资源；
       (3)关闭代码的逻辑比较冗长，不应该是正常的业务逻辑需要关注的；
    
    对于实现AutoCloseable接口的类的实例，将其放到try后面，在try结束的时候，
    会自动将这些资源关闭（调用close方法）。
     
    3个关键点：
    1、带资源的try语句管理的资源必须实现了AutoCloseable接口的类的对象。
    2、在try代码中声明的资源被隐式声明为fianl。
    3、通过使用分号分隔每个声明可以管理多个资源。

#2.stream方法
    void close() throws Exception;
    
#3.用法示例
```java
public class Test{
private static void autoClosable() throws IOException {
		//jdk1.7版本流会自动关闭，实现了AutoClosable接口
		try(
				FileInputStream fis = new FileInputStream("xxx.txt");
				FileOutputStream fos = new FileOutputStream("hhh.txt");
				MyAutoClosable closable = new MyAutoClosable();
		){
			int len = -1;
			while((len=fis.read())!=-1) {
				fos.write(len);
			}
		}
	}
}
```
#4.源码
```java
public interface AutoCloseable {
    /**
     * 在使用try-catch语法创建的资源抛出异常后，JVM自动调用close方法进行资源释放，
     * 正常退出try-block时候也会调用close方法
     */
    void close() throws Exception;
}
```

