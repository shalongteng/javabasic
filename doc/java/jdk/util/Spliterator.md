#1.spliterator概述
    splitable iterator可分割迭代器
    为了并行遍历元素而设计的一个迭代器，jdk1.8中集合都实现了spliterator
#2.spliterator方法
    boolean tryAdvance(Consumer<? super T> action);
        单个对元素执行给定的动作，有剩下元素未处理返回true，否则返回false
        
    Spliterator<T> trySplit()
        分割迭代器，每调用一次，将原来的迭代器等分为两份，并返回索引靠前的那一个子迭代器。
    
    forEachRemaining(Consumer<? super E> action)
        通过action批量消费所有的未迭代的数据。
    
    default long getExactSizeIfKnown() 
        如果知道确切大小，就返回
        
    default boolean hasCharacteristics(int characteristics)
        是否保护某一个特征
        
    
    int characteristics();
        返回当前实例的一个特征集
        
        ORDERED = 0x00000010;
            表示迭代器需要按照其原始顺序迭代其中元素的
        
        DISTINCT = 0x00000001;
            迭代器中的元素是没有重复的
        
        SORTED = 0x00000004;
            迭代器是按照某种方式排序的顺序迭代其中元素的
            如果是sorted一定也是ordered
        
        SIZED = 0x00000040;
            表示迭代器将要迭代的元素的个数是可计数的，有界的
        
        NONNULL = 0x00000100;
            迭代器迭代的元素是没有值为`null`的
        
        IMMUTABLE = 0x00000400;
            迭代器迭代的元素是不可改变的，也不可以增加、替换和删除
        
        CONCURRENT = 0x00001000;
            表示迭代器的数据源是线程安全的
        
        SUBSIZED = 0x00004000;
            表示当前迭代器所有的子迭代器（直接的或者间接的），都是`SIZED`和`SUBSIZED`的
    
#3.子接口 OfPrimitive
    对基本类型做的特殊实现。
    
    OfPrimitive子接口
        OfInt
        OfLong
        OfDouble
#4.源码
```java
public interface AutoCloseable {
    /**
     * 在使用try-catch语法创建的资源抛出异常后，JVM自动调用close方法进行资源释放，
     * 正常退出try-block时候也会调用close方法
     */
    void close() throws Exception;
}
```

