#2.Date概述
    Date 表示特定的时刻（instant），精确到毫秒。
    implements java.io.Serializable, Cloneable, Comparable<Date>
    
    两个额外功能 --- 已经被废弃
        允许将时间解释为年、月、日、时、分、秒。
        能格式化和转换时间字符串
        
    对于时间的计算基本都是有Calendar类或相关的类完成
    DateFormat类（用作格式和时间支付串转换）之间的转换
#2.属性
    fastTime
        当前时间的毫秒表示(从1970.1.1开始算)
    BaseCalendar gcal
        Gregorian calendar
    BaseCalendar jcal
        julian calendar
    BaseCalendar.Date cdate 
#3.构造器
    使用毫秒值的构造器
        public Date(long date) {
            //long 值是从1970年1.1 零点开始计算的  单位是毫秒
            fastTime = date;
        }
    使用年月日时分秒的构造器
        public Date(int year, int month, int date, int hrs, int min, int sec)
    使用gmt 时间字符串的构造器
        public Date(String s)
#4.常用的方法
    after，before
        比较两个date的long fasttime值
    long getTime()
        获取date的fasttime
    void setTime(long time)
    
#5.java8新加的方法
    static Date from(Instant instant）
        instant转date
    Instant toInstant()
        date转instant

    
