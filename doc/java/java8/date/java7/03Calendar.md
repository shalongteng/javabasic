#1.calendar概述
     int    getActualMaximum(int field) 
              给定此 Calendar 的时间值，返回指定日历字段可能拥有的最大值。
    calendar 类是一个抽象类，
        它为特定时间与一组诸如 YEAR、MONTH、DAY_OF_MONTH、HOUR 等 日历字段之间的转换提供了一些方法，
        为操作日历字段（例如获得下星期的日期）提供了一些方法。
        瞬间可用毫秒值来表示，它是距历元（即格林威治标准时间 1970 年 1 月 1 日的 00:00:00.000，格里高利历）的偏移量。
#1.常用方法
    1.与date转换
         getTime() 
              返回一个表示此 Calendar 时间值（从历元至现在的毫秒偏移量）的 Date 对象。
         setTime(Date date) 
              使用给定的 Date 设置此 Calendar 的时间。
              
              
    2.set get 方法
         set(int field, int value) 
                  将给定的日历字段设置为给定值。
         set(int year, int month, int date, int hourOfDay, int minute, int second) 
                  设置字段 YEAR、MONTH、DAY_OF_MONTH、HOUR、MINUTE 和 SECOND 的值。
         setFirstDayOfWeek(int value) 
                  设置一星期的第一天是哪一天；例如，在美国，这一天是 SUNDAY，而在法国，这一天是 MONDAY。
                  
         setTimeInMillis(long millis) 
                  用给定的 long 值设置此 Calendar 的当前时间值。
         setTimeZone(TimeZone value) 
                  使用给定的时区值来设置时区。
                                     
         get(int field) 
                  返回给定日历字段的值。
         getActualMinimum(int field) 
                  给定此 Calendar 的时间值，返回指定日历字段可能拥有的最小值。 
         getTimeInMillis() 
               返回此 Calendar 的时间值，以毫秒为单位。    
         getTimeZone() 
              获得时区。
                                   
     3.操作时间的方法
         add(int field, int amount) 
                  根据日历的规则，为给定的日历字段添加或减去指定的时间量。
         clear() 
              将此 Calendar 的所日历字段值和时间值（从历元至现在的毫秒偏移量）设置成未定义。
         clear(int field) 
              将此 Calendar 的给定日历字段值和时间值（从历元至现在的毫秒偏移量）设置成未定义。                  
         
     
     4.获取实例
        Calendar getInstance() 
           使用默认时区和语言环境获得一个日历。             
     5.其他方法             
         boolean    after(Object when) 
                  判断此 Calendar 表示的时间是否在指定 Object 表示的时间之后，返回判断结果。
         boolean    before(Object when) 
                  判断此 Calendar 表示的时间是否在指定 Object 表示的时间之前，返回判断结果。
    
         Object clone() 
                  创建并返回此对象的一个副本。
       
         int    getFirstDayOfWeek() 
                  获取一星期的第一天；例如，在美国，这一天是 SUNDAY，而在法国，这一天是 MONDAY。
    
         String toString() 
                  返回此日历的字符串表示形式。
