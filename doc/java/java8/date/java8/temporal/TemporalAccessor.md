#1、
    定义对临时对象（例如日期，时间，偏移或这些的某种组合。）的只读访问的接口 
    
    这是日期，时间和偏移对象的基本接口类型。 它由那些可以提供信息的类实现，如字段或queries 。
    大多数日期和时间信息可以表示为数字。 这些使用TemporalField建模，使用TemporalField保持long以处理大值
#1、
    get​(TemporalField field)	
        获取指定字段的 int 。
    long	getLong​(TemporalField field)	
        获取指定字段的 long 。
    boolean	isSupported​(TemporalField field)	
        检查是否支持指定的字段。
    default <R> R	query​(TemporalQuery<R> query)	
        查询此日期时间。
    default ValueRange	range​(TemporalField field)	
        获取指定字段的有效值范围。
#1、