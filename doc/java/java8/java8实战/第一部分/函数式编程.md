#1.行为参数化
    概念：
        一个方法接受不同的行为作为参数，完成不同行为的能力。
        
    传递代码
        就是将新行为作为参数传递给方法
        
    java8之前行为参数化：
        为接口声明许多只用一次的实体类而造成的啰嗦代码
        在Java 8之前可以用匿名类来减少。
        
    java8行为参数化：
        lambda表达式
#2.Lambda表达式
    lambda管中窥豹
        可以把Lambda表达式理解为匿名函数
        
        函数——因为Lambda函数不像方法那样属于某个特定的类。但和方法一样
        Lambda有参数列表、函数主体、返回类型，还可能有可以抛出的异常列表。
        
    组成部分：  (parameters) -> expression
        参数列表——这里它采用了Comparator中compare方法的参数，两个Apple。 
        箭头——箭头->把参数列表与Lambda主体分隔开。
        Lambda主体——比较两个Apple的重量。表达式就是Lambda的返回值了
        
    使用场景：
        在接受函数式接口做为参数的方法是使用
            只有一个方法的接口
#3.方法引用
    方法引用就是让你根据已有的方法实现来创建Lambda表达式.
    对象的引用，方法的引用
    
    方法引用大致分四种：
        静态方法（Integer::parseInt）。
            String::valueOf，等价于 s -> String.valueOf(s)
            省略了方法参数
        
        对象实例（expensiveTransaction::getValue）。
            指的是，你在Lambda中调用一个已经存在的外部对象中的方法
            ()->expensiveTransaction.getValue()可以写作expensiveTransaction::getValue
            实例方法省略了方法参数
            
        普通方法（String::length）。
            (String s) -> s.toUppeCase()可以写作String::toUpperCase
            省略了第一个调用者参数,以及n个方法参数。
            
            lambda表达式的第一个参数，就是调用者
            lambda表达式的后续参数，作为方法参数
            主要看函数式接口需要几个参数。
        
        构造方法引用
            String::new
    
