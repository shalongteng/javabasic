#1、StreamShape
    在stream里，通过对流的形状进行描述来调用不同的实现类（对应的枚举类为StreamShape），
    从而完成stream根据类型区分的的个性化定制。
#2、枚举值
    REFERENCE
        对象引用
    INT_VALUE
        int值
    LONG_VALUE
        long值
    DOUBLE_VALUE
        double值
#3、源码
```java
/**
 * @since 1.8
 */
enum StreamShape {
    /**
     * The shape specialization corresponding to {@code Stream} and elements
     * that are object references.
     */
    REFERENCE,
    /**
     * The shape specialization corresponding to {@code IntStream} and elements
     * that are {@code int} values.
     */
    INT_VALUE,
    /**
     * The shape specialization corresponding to {@code LongStream} and elements
     * that are {@code long} values.
     */
    LONG_VALUE,
    /**
     * The shape specialization corresponding to {@code DoubleStream} and
     * elements that are {@code double} values.
     */
    DOUBLE_VALUE
}

```