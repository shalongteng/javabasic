#1.stream概述
    中间操作 返回一个流。
        无状态（Stateless）操作
            元素的处理不受之前元素的影响。
            
        有状态（Stateful）操作
            只有拿到所有元素之后才能继续下去。
    终端操作
        终端操作会消耗流，以产生一个最终结果。


        
#2.方法分类
    1.查询条件 where
         Stream<T> filter(Predicate<? super T> predicate);
            过滤操作 参数是一个predicate
         boolean anyMatch(Predicate<? super T> predicate);
            任意一个元素成功，返回true
         boolean allMatch(Predicate<? super T> predicate);
            所有的都是，返回true
         boolean noneMatch(Predicate<? super T> predicate);
            所有的都不是，返回true
            
        查找操作对应的终端操作
            Optional<T> findFirst();
                返回找到的第一个值
            Optional<T> findAny();
                一般也是返回找到的第一个值
                
    2.查询具体的列 select name，sex  from
         map(Function<? super T, ? extends R> mapper);
            映射操作  参数是一个function
            还有对应几个特例 mapToInt，mapToLong，mapToDouble
                
         flatMap(Function<? super T, ? extends Stream<? extends R>> mapper);   
            如果map里返回的也是一个流，此时可以使用flatmap 合并流.
            扁平流：可以将多个源合成一个源。
            
    3.转换成具体的流
        mapToInt,mapToLong,mapToDouble
        flatMapToInt,flatMapToLong,flatMapToDouble
        
    4.常用的中间操作
        Stream<T> distinct();
            去重
        Stream<T> sorted();   Stream<T> sorted(Comparator<? super T> comparator);
            排序
            可以按照默认排序，也可以传递一个比较器来进行排序。
        Stream<T> peek(Consumer<? super T> action);
            查看中间操作的结果,
            返回一个stream，foreach不返回值。
        Stream<T> limit(long maxSize);
            取前几条
        Stream<T> skip(long n);
            跳过前几条，剩下的才开始处理
           
    5.常用的终端操作：
        forEach：  
            forEach(Consumer<? super T> action);
                可能会有副作用。可能会修改流中的元素。
            forEachOrdered(Consumer<? super T> action)
                无论串行还是并行，都会按照顺序来遍历。
        toArray
            Object[] toArray()
            <A> A[] toArray(IntFunction<A[]> generator)
            
        reduce 归约
            Optional<T> reduce(BinaryOperator<T> accumulator)
            T reduce(T identity, BinaryOperator<T> accumulator) 
            reduce(U identity,BiFunction<U, ? super T, U> accumulator,BinaryOperator<U> combiner);
            
        collect 将流收集成一个值或 一个集合   
            <R> R collect(Supplier<R> supplier,BiConsumer<R, ? super T> accumulator,BiConsumer<R, R> combiner);
                相当于自己实现收集器
            <R, A> R collect(Collector<? super T, A, R> collector)
                将实现好的收集器传入进去
            
        min,max,count
            都可以使用reduce 实现。
            
    5.生成流方法：
        Stream<T> of(T... values)
            根据数组生成流
        Stream<T> of(T t)
            根据一个值，生成流
            
        iterate
            根据一种规则生成一系列的值。
        
        generate
        
        concat
            将两个流合并成一个流。
        
        build方法
#3.stream内部接口 Builder
```java
 public interface Builder<T> extends Consumer<T> {
        @Override
        void accept(T t);

        default Builder<T> add(T t) {
            accept(t);
            return this;
        }

        Stream<T> build();

    }
```
#4.源码
     