    https://www.cnblogs.com/cxy2020/p/14099468.html
#1.ReferencePipeline(引用管道）概述
    中间管道阶段或源阶段实现的抽象基类（将流的源和中间操作统一在一个类）

#2.构造器
    构造源的Constructor
        ReferencePipeline(Supplier<? extends Spliterator<?>> source,
                          int sourceFlags, boolean parallel) {
            super(source, sourceFlags, parallel);
        }
        ReferencePipeline(Spliterator<?> source,
                          int sourceFlags, boolean parallel) {
            super(source, sourceFlags, parallel);
        }
        
    构造中间操作的Constructor
        ReferencePipeline(AbstractPipeline<?, P_IN, ?> upstream, int opFlags) {
            super(upstream, opFlags);
        }

#3.内部类
    class Head<E_IN, E_OUT> extends ReferencePipeline<E_IN, E_OUT>
        表示流的源阶段，Head没有previousStage
        
    class StatelessOp<E_IN, E_OUT> extends ReferencePipeline<E_IN, E_OUT> 
        无状态操作，针对一个流的无状态的中间阶段，继承了 ReferencePipeline
        
        构造方法
            StatelessOp(AbstractPipeline<?, E_IN, ?> upstream,StreamShape inputShape,int opFlags)
            构造一个新的流，给新的流追加一个无状态操作
    class StatefulOp<E_IN, E_OUT> extends ReferencePipeline<E_IN, E_OUT>
        有状态操作 需要拿到全部元素
        
        构造方法
            StatefulOp(AbstractPipeline<?, E_IN, ?> upstream,StreamShape inputShape,int opFlags)
            构造一个新的流，给新的流添加一个有状态的操作
#4.常用方法 
    map方法
        将回调函数mapper包装到一个Sink当中。由于Stream.map()是一个无状态的中间操作，所以map()方法
        返回了一个StatelessOp内部类对象（一个新的Stream），调用这个新Stream的opWripSink()方法将得到
        一个包装了当前回调函数的Sink。
    
        
    filter
        返回了一个新的 StatelessOp 对象。new StatelessOp 将会调用父类 AbstractPipeline 的构造函数，
        这个构造函数将前后的 Stage 联系起来，生成一个 Stage 链表
        
    max(Comparator.naturalOrder())
        终结操作，会生成一个最终的 Stage，通过这个 Stage 触发之前的中间操作，从最后一个Stage开始，递归产生一个Sink链。
#5.终端方法
    ReferencePipeline中的foreach调用流程
        前提：流的源头和中间操作已经构造完成。
        使用consumer构造一个terminalOp（ForeachOp）
        调用刚刚构造的ForEachOp.evaluateSequential(PipelineHelper helper,Spliterator spliterator)
        
        abstractPipeline.wrapAndCopyInto(this, spliterator).get();
            此方法就是 wrapsink和copyInto 组合体
            此处get返回的是null，foreach是没有返回值的
            
        调用wrapSink(Objects.requireNonNull(sink)
            此方法调用具体的例如map中实现的OpwrapSink方法，使用Sink.ChainedReference 中downstream属性
            将sink组合成了一个链。filter sink -> map sink -> foreach sink
        
            使用sink的串联的有
                重写sink的accept方法
                filter
                map
                peek
                unordered
        调用copyInto方法（模板方法），处理元素
            此方法会遍历spliterator中数据，每个数据都会沿着sink链，依次向下执行。
            
            
            
     

#6.源码
     