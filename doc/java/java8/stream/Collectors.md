#1.Collectors概述
    工厂类，提供常见的收集器。提供的大都是静态方法。
    
    静态方法两种实现：
        通过CollectorImpl实现
        通过reducing实现，而reducing又是通过CollectorImpl实现的。
    
#2.Collectors方法
    中间操作
        中间操作可以链接起来，将一个流转换为另一个流。
        
    终端操作
        reducing(BinaryOperator<T> op)
            一个参数 
            内部有一个OptionalBox 内置了一个value变量，作为初始的identity。
        reducing(T identity, BinaryOperator<T> op) 
            两个参数
            执行operator的规约方法。
        reducing(U identity,Function<? super T, ? extends U> mapper,BinaryOperator<U> op)
            三个参数
            流中元素可以先map映射，然后再执行operator的规约方法。
        
#3.Collectors源码
