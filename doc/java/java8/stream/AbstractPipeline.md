#1.AbstractPipeline概述
    AbstractPipeline可以理解为双向链表链接上游管道和下游管道。每个管道里面的属性记录了上游管道的信息，
    当执行终止操作时可以通过管道信息找到上游管道信息
#2.属性
    previousStage
        上游的 pipeline，源阶段置 null
    sourceStage
        指向管道头的反指向链，源阶段为自身
    sourceOrOpFlags
    combinedFlags
    depth
        当前管道对象与流的源(串行)或前状态(并行)之间，中间操作的个数
        在管道准备进行计算的时间点有效
    parallel
        是否并行，只在流的源阶段有效
    sourceSpliterator
        源的 spliteraror，只在管道头有效
        在管道被消费前，若为“非空”，则 sourceSupplier 必须为 null
        在管道被消费后，若为“非空”，则将其置 null
    sourceSupplier
        源的 supplier，只在管道头有效
        在管道被消费前，若为“非空”，则 sourceSpliterator 必须为 null
        在管道被消费后，若为“非空”，则将其置 null
#2.构造器
    构造流管道的头
        AbstractPipeline(Supplier<? extends Spliterator<?>> source,
                         int sourceFlags, boolean parallel) {
            this.previousStage = null;
            this.sourceSupplier = source;
            this.sourceStage = this;
            this.sourceOrOpFlags = sourceFlags & StreamOpFlag.STREAM_MASK;
            // The following is an optimization of:
            // StreamOpFlag.combineOpFlags(sourceOrOpFlags, StreamOpFlag.INITIAL_OPS_VALUE);
            this.combinedFlags = (~(sourceOrOpFlags << 1)) & StreamOpFlag.INITIAL_OPS_VALUE;
            this.depth = 0;
            this.parallel = parallel;
        }
        AbstractPipeline(Spliterator<?> source,
                         int sourceFlags, boolean parallel) {
            this.previousStage = null;
            this.sourceSpliterator = source;
            this.sourceStage = this;
            this.sourceOrOpFlags = sourceFlags & StreamOpFlag.STREAM_MASK;
            // The following is an optimization of:
            // StreamOpFlag.combineOpFlags(sourceOrOpFlags, StreamOpFlag.INITIAL_OPS_VALUE);
            this.combinedFlags = (~(sourceOrOpFlags << 1)) & StreamOpFlag.INITIAL_OPS_VALUE;
            this.depth = 0;
            this.parallel = parallel;
        }
    构造中间操作的Constructor
        AbstractPipeline(AbstractPipeline<?, E_IN, ?> previousStage, int opFlags) {
            if (previousStage.linkedOrConsumed)
                throw new IllegalStateException(MSG_STREAM_LINKED);
            previousStage.linkedOrConsumed = true;
            previousStage.nextStage = this;
    
            this.previousStage = previousStage;
            this.sourceOrOpFlags = opFlags & StreamOpFlag.OP_MASK;
            this.combinedFlags = StreamOpFlag.combineOpFlags(opFlags, previousStage.combinedFlags);
            this.sourceStage = previousStage.sourceStage;
            if (opIsStateful())
                sourceStage.sourceAnyStateful = true;
            this.depth = previousStage.depth + 1;
        }
#3.抽象方法
    abstract Sink<E_IN> opWrapSink(int flags, Sink<E_OUT> sink);
        接收一个接收操作结果的 Sink，并返回一个接收当前操作的输入类型的元素并执行操作的 Sink，将结果传递到提供的 Sink 中
        实现类可能使用 flag 参数去优化 sink 的包装
        例如，如果输入已经是 DISTINCT ，那么 distinct() 方法的实现只需要返回 sink
#3.stream内部接口 Builder

#4.源码
     