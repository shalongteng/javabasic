#1.Collector接口概述
    他是一个可变的汇聚操作，将输入元素累计到一个可变结果容器中，
    T是流中要收集的项目的泛型。
    A是累加器的类型，累加器是在收集过程中用于累积部分结果的对象。
    R是收集操作得到的对象（通常但并不一定是集合）的类型。
    
#2.Collector 主要方法
     supplier方法
        创建新的结果容器
        
     accumulator方法
         返回执行归约操作的函数,将元素添加到结果容器
     combiner方法
         返回一个供归约操作使用的函数，如果是parallelstream，会调用combiner方法把中间结果容器合并。
         
     finisher方法
        将累加器对象转换为整个集合操作的最终结果
         

     characteristics方法
        CONCURRENT
            并行的，需要多个线程并行操作中间结果容器。
            如果没有次标志，多线程每个线程都创建一个结果容器，最后把结果容器合并起来。
            有次标志，只创建一个结果容器，多个线程同时调用，此时可能会存在线程安全问题。
        UNORDERED
            无序的
            
        IDENTITY_FINISH
            表示中间结果容器可以强制转成最后结果容器，不报错。
            有了此标志，将不会调用finisher方法。
     
#3.源码
```java
public interface Collector<T, A, R> { 
     Supplier<A> supplier(); 
     BiConsumer<A, T> accumulator();    
     Function<A, R> finisher(); 
     BinaryOperator<A> combiner(); 
     Set<Characteristics> characteristics(); 
}
```
