#1、TerminalOp概述
    终端操作的父接口
    
    输入参数
        接受流作为输入参数
    输出
        返回一个结果
        不返回结果
            产生副作用 例如foreach

    
#2、主要方法
     default StreamShape inputShape() { return StreamShape.REFERENCE; }
        获取输入类型
     default int getOpFlags() { return 0; }
        获取操作标识，用于描述操作如何处理流中的元素
        例如：短路、以指定顺序执行
        
     default <P_IN> R evaluateParallel
        执行并行操作
     <P_IN> R evaluateSequential
        执行串行操作
#3、实现类
    ForEachOp
        ForEachOps 中的静态内部类
    FindOp
        FindOps 中的静态内部类
    MatchOp
        MatchOps 中的静态内部类
    ReduceOp
        ReduceOps 中的静态内部类
