#1、Sink概述
    https://www.cnblogs.com/CarpenterLee/archive/2017/03/28/6637118.html
    Sink[处理->转发]模型
    
    每个Stage都会将自己的操作封装到一个Sink里，前一个Stage只需调用后一个Stage的accept()方法即可，并不需要知道其内部是如何处理的。
    当然对于有状态的操作，Sink的begin()和end()方法也是必须实现的。比如Stream.sorted()是一个有状态的中间操作，
    其对应的Sink.begin()方法可能创建一个乘放结果的容器，而accept()方法负责将元素添加到该容器，最后end()负责对容器进行排序。
    对于短路操作，Sink.cancellationRequested()也是必须实现的，比如Stream.findFirst()是短路操作，只要找到一个元素，
    cancellationRequested()就应该返回true，以便调用者尽快结束查找。Sink的四个接口方法常常相互协作，共同完成计算任务。
    实际上Stream API内部实现的的本质，就是如何重载Sink的这四个接口方法。
    
    有了Sink对操作的包装，Stage之间的调用问题就解决了，执行时只需要从流水线的head开始对数据源依次调用每个Stage对应的
    Sink.{begin(), accept(), cancellationRequested(), end()}方法就可以了。
#2、内部接口 
    interface OfInt extends Sink<Integer>, IntConsumer
    interface OfLong extends Sink<Long>, LongConsumer
    interface OfDouble extends Sink<Double>, DoubleConsumer
#2、内部类
    static abstract class ChainedInt<E_OUT> implements Sink.OfInt
    static abstract class ChainedLong<E_OUT> implements Sink.OfLong
    static abstract class ChainedDouble<E_OUT> implements Sink.OfDouble
    
    static abstract class ChainedReference<T, E_OUT> implements Sink<T> 
        引用链
#2、默认方法
    default void begin(long size) {}
        开始遍历元素之前调用该方法，通知Sink做好准备。
    default void end() {}
        所有元素遍历完成之后调用，通知Sink没有更多的元素了。
    default boolean cancellationRequested() {
            return false;
    }
        是否可以结束操作，可以让短路操作尽早结束。
    void accept(T t)
        遍历元素时调用，接受一个待处理元素，并对元素进行处理。Stage把自己包含的操作和回调方法封装到该方法里，前一个Stage只需要调用当前Stage.accept(T t)方法就行了。