#1、PipelineHelper概述
    描述流管道各种各样的信息
    
    用于执行流管道的辅助类
    用于在一个地方捕获所有关于流管道的信息（输出种类、中间操作、流标识、并行 等）
    
    PipelineHelper 描述了一个流管道的初始分块，包括它的源、中间操作
    和可能额外附加的关于终止（或有状态）操作的信息，该操作跟进上一个由当前 PipelineHelper 描述的中间操作
    
    PipelineHelper 会被传递给 evaluateParallel、evaluateSequential，与 opEvaluateParallel 等方法
    
    使其能够使用 PipelineHelper 去访问关于管道的各种信息
#1、主要方法
    abstract StreamShape getSourceShape()
    abstract int getStreamAndOpFlags()
    abstract<P_IN> long exactOutputSizeIfKnown(Spliterator<P_IN> spliterator);
    
    abstract<P_IN, S extends Sink<P_OUT>> S wrapAndCopyInto(S sink, Spliterator<P_IN> spliterator);
        wrapSink 和 copyInto  组合方法
        将当前 PipelineHelper 描述的管道阶段应用到所提供的 Spliterator，并将结果发送给提供的 Sink
        
    
    abstract<P_IN> Sink<P_IN> wrapSink(Sink<P_OUT> sink);
        使用sink内部属性downStream，把sink 串联起来
        接收一个 Sink，此 Sink 本身会接收 PipelineHelper 输出类型的元素
    abstract<P_IN> void copyInto(Sink<P_IN> wrappedSink, Spliterator<P_IN> spliterator);
        将从 Spliterator 中获取到的元素推到提供的 Sink 中
        若流管道知道有“短路”的阶段，就会进行短路判断
        @implSpec
        当前方法符合 Sink 的协议：
        先调用 begin 方法
        然后 accept 方法
        当所有元素推送完毕后，调用 end 方法
        

#1、
#1、