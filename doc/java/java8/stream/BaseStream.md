#1.BaseStream概述
    BaseStream<T, S extends BaseStream<T, S>>
        @Param T :  流中的元素类型
        @Param S :  一个BaseStream的实现类
        
    stream的跟接口。
#2.stream方法
    Iterator<T> iterator();
        返回此流的元素的迭代器。
    Spliterator<T> spliterator();
        通过流返回一个分隔迭代器
    boolean isParallel();
        返回此流（如果要执行终端操作）是否将并行执行。
        
    S sequential();
        返回串行的等效流。
    S parallel();
        返回并行的等效流。
    S unordered();
        返回等效流，即 unordered 。
        
    S onClose(Runnable closeHandler);
        返回具有附加关闭处理程序的等效流。
    void close();
        关闭此流，导致调用此流管道的所有关闭处理程序。
#3.源码
