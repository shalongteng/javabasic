#1、IntFunction概述
    这些接口的目标是允许直接使用原始类型.可以节省自动装箱和自动拆箱消的耗,
    从而使这些接口(以及依赖它们的相关IntStream,LongStream和DoubleStream)更加高效.
    
    例如：
        使用函数<Integer,R>有一个方法接受一个Integer并产生一个R类型的结果,
        使用IntFunction<R>它有一个方法,它接受一个int并产生一个类型为R的结果.
        将int传递给该函数,可避免在将int传递给Function<Integer,R>的方法时发生的装箱.
#2、IntFunction主要方法
    R apply(int value);
        接收int值，返回R对象
        
#3、其他类似的
    IntPredicate
        boolean test(int value);
            接受int值，返回boolean值
            
    IntConsumer
        void accept(int value);
            消费一个int值
            
    IntSupplier
        int getAsInt();
            获取一个int值
            
    IntBinaryOperator
        int applyAsInt(int left, int right);
            接受两个int参数，返回一个int值。
            因为BinaryOperator本身就是BiFunction一个特殊形式，所以不会有IntBiFunction
            
    IntToDoubleFunction
        double applyAsDouble(int value);
            接受一个int参数，返回一个double值
            
    IntToLongFunction
        long applyAsLong(int value);
            接受一个int参数，返回一个long值   
               
    IntUnaryOperator
        int applyAsInt(int operand);       
            接受一个int值，返回一个int值。
             
#4、IntFunction源码

```java
@FunctionalInterface
public interface IntFunction<R> {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    R apply(int value);
}
```

