#1、ToIntFunction概述
    这类接口参数还是泛型，返回值是基本类型。
#2、IntFunction主要方法
    int applyAsInt(T value);
        接收int值，返回T对象
        
#3、其他类似的
    ToIntBiFunction
        int applyAsInt(T t, U u);
            接受两个参数，返回一个int值
            
    ToLongFunction
        long applyAsLong(T value);
            接受T值，返回long值
            
    ToLongBiFunction
         long applyAsLong(T t, U u);
            接受两个参数，返回long值   
             
    ToDoubleFunction
        ToDoubleFunction
            接受T值，返回double值
            
    ToDoubleBiFunction
         double applyAsDouble(T t, U u);
            接受两个参数，返回double值
             
#4、IntFunction源码

```java
@FunctionalInterface
public interface ToIntFunction<T> {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    int applyAsInt(T value);
}
```

