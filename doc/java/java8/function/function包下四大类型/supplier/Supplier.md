#1、Supplier概述
    语义：
        供应者
#2、Supplier方法
    T get() 
        用来获取一个泛型参数指定类型的对象数据
#3、Supplier源码

```java
@FunctionalInterface
public interface Supplier<T> {

    /**
     * Gets a result.
     *
     * @return a result
     */
    T get();
}
```

