#1、Function概述
    使用场景：
        一般用来提取某一个值。比如stream流的 map方法
    
#2、Function主要方法
    R apply(T t);
        接收T对象，返回R对象
    default <V> Function<V, R> compose(Function<? super V, ? extends T> before)
        组合函数，相当于  f(g(x))
        先做传入的Function类型的参数的apply操作，再做当前这个接口的apply操作
        
        第一个函数 参数V 返回 T
        第二个函数 参数T 返回 R
     
    default <V> Function<T, V> andThen(Function<? super R, ? extends V> after)
        先做本接口的apply操作，再做传入的Function类型的参数的apply操作
        相当于 g(f(x))
    
    static <T> Function<T, T> identity() 
        返回自身的 function  相当于 a -> a    
#3、Function源码

```java
@FunctionalInterface
public interface Function<T, R> {

    R apply(T t);
    /**
       * 先做传入的Function类型的参数的apply操作，再做当前这个接口的apply操作
       * V表示这个Function类型的参数的传入参数类型，也就是本接口的T类型
       * @param before
       * @return
       */
    default <V> Function<V, R> compose(Function<? super V, ? extends T> before) {
        Objects.requireNonNull(before);
        return (V v) -> apply(before.apply(v));
    }
     /**
       * 先做本接口的apply操作，再做传入的Function类型的参数的apply操作
       * @param after
       * @return
       */
    default <V> Function<T, V> andThen(Function<? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return (T t) -> after.apply(apply(t));
    }
    /**
     * 静态方法表示，这个传入的泛型参数T的本身
       返回一个输出跟输入一样的Lambda表达式对象
     * @return
     */
    static <T> Function<T, T> identity() {
        return t -> t;
    }
}
```

