#1.BinaryOperator概述
    BinaryOperator继承了BiFunction，多了二个静态方法minBy和maxBy。
    
    BiFunction的一个特殊例子（BiFunction三个泛型相同时候），接收两个参数，产生一个结果，
    只是它的三个参数都是同一个数据类型，这个函数式接口就是BinaryOperator
    
#2.BinaryOperator方法
    static <T> BinaryOperator<T> minBy(Comparator<? super T> comparator)
        返回了一个BiFunction对应的Lambda，Lambda内部使用传入的comparator比较器比较，
        然后在进一步处理返回最大值还是最小值。
        
    static <T> BinaryOperator<T> maxBy(Comparator<? super T> comparator)
#3.BinaryOperator源码
```java
@FunctionalInterface
public interface BinaryOperator<T> extends BiFunction<T,T,T> {
  /**
     * 通过比较器Comparator来比较两个元素中较小的一个作为返回值返回。
     * @param <T> 比较器的输入参数的类型
     * @param comparator 用来比较两个值的Comparator
     * @return 通过比较器Comparator来比较两个元素中较小的一个作为返回值返回。
     */
    public static <T> BinaryOperator<T> minBy(Comparator<? super T> comparator) {
        Objects.requireNonNull(comparator);
        return (a, b) -> comparator.compare(a, b) <= 0 ? a : b;
    }

    /**
     * Returns a {@link BinaryOperator} which returns the greater of two elements
     */
    public static <T> BinaryOperator<T> maxBy(Comparator<? super T> comparator) {
        Objects.requireNonNull(comparator);
        return (a, b) -> comparator.compare(a, b) >= 0 ? a : b;
    }
}
```