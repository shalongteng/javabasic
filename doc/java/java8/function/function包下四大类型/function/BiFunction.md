#1.BiFunction概述
    可以理解为Function的一种扩展, Function接口接收一个参数, 返回一个值; 
    BiFunction接口接受两个参数, 返回一个值
    
    Bi是Bidirectional【双向】的简写
#2.BiFunction方法
    R apply(T t, U u);
        接受两个参数，返回R
#3.BiFunction源码
```java
@FunctionalInterface
public interface BiFunction<T, U, R> {

    /**
     * Applies this function to the given arguments.
     */
    R apply(T t, U u);

    /**
     * Returns a composed function that first applies this function to
     * @throws NullPointerException if after is null
     */
    default <V> BiFunction<T, U, V> andThen(Function<? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return (T t, U u) -> after.apply(apply(t, u));
    }
}
```