#1.UnaryOperator概述
    unary：一元的
    
    只接收一个泛型参数T，继承Function接口，也就是说，传入泛型T类型的参数，
    调用apply后，返回也T类型的参数；定义了一个静态方法，返回泛型对象的本身；
    
    Function接口只有一个泛型的特殊形式。
#2.UnaryOperator方法
    static <T> UnaryOperator<T> identity()
        接受两个参数，返回R
#3.UnaryOperator源码
```java
@FunctionalInterface
public interface UnaryOperator<T> extends Function<T, T> {
    /**
     * 
     */
    static <T> UnaryOperator<T> identity() {
        return t -> t;
    }
}
```