#1、Consumer概述
    Consumer的语义
        消费的意思,类似于队列的消费者。
        
    使用场景：
        主要是对入参做一些列的操作，在stream里，主要是用于forEach；
        内部迭代的时候，对传入的参数，做一系列的业务操作，没有返回值；
#2、Consumer方法
    void accept(T t);
        对传入的参数，做一系列的业务操作，没有返回值；
        
#3、Consumer源码

```java
@FunctionalInterface
public interface Consumer<T> {
 
    
    void accept(T t);
 
    /**传入一个Consumer类型的参数，
	 *他的泛型类型，跟本接口是一致的T，先做本接口的accept操作，
	 *然后在做传入的Consumer类型的参数的accept操作
	*/
    default Consumer<T> andThen(Consumer<? super T> after) {
        Objects.requireNonNull(after);
        return (T t) -> { accept(t); after.accept(t); };
    }
}
```

