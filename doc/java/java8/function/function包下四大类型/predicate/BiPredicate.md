#1.BiPredicate概述
    Predicate接口主要用来判断一个参数是否符合要求。
    是函数式接口，既只有一个抽象方法
#2.抽象方法
    boolean test(T t, U u);
        接受两个参数，返回一个bool值
#3.源码
```java
@FunctionalInterface
public interface BiPredicate<T, U> {
    /**
     * Evaluates this predicate on the given arguments.
     */
    boolean test(T t, U u);

    /**
     * Returns a composed predicate that represents a short-circuiting logical
     */
    default BiPredicate<T, U> and(BiPredicate<? super T, ? super U> other) {
        Objects.requireNonNull(other);
        return (T t, U u) -> test(t, u) && other.test(t, u);
    }

    /**
     * Returns a predicate that represents the logical negation of this
     * predicate
     */
    default BiPredicate<T, U> negate() {
        return (T t, U u) -> !test(t, u);
    }

    /**
     * Returns a composed predicate that represents a short-circuiting logical
     */
    default BiPredicate<T, U> or(BiPredicate<? super T, ? super U> other) {
        Objects.requireNonNull(other);
        return (T t, U u) -> test(t, u) || other.test(t, u);
    }
}
```
