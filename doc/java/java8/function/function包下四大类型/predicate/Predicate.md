#1.Predicate概述
    Bi是Bidirectional【双向】的简写
#2.抽象方法
    boolean test(T t);
#3.默认方法
    既然是条件判断，就会存在与、或、非三种常见的逻辑关系
        default Predicate<T> and(Predicate<? super T> other)
        default Predicate<T> or(Predicate<? super T> other)
        default Predicate<T> negate()
        
    判等
        static <T> Predicate<T> isEqual(Object targetRef)
