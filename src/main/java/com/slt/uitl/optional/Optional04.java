package com.slt.uitl.optional;

import java.util.Optional;

/**
 * map,flatmap
 */
public class Optional04 {
    public static void main(String[] args) {
        Optional<String> test = Optional.ofNullable("abcD");
        Optional<String> empty = Optional.empty();
        String s = test.orElse("hello world");
        String s1 = empty.orElse("hello world");
        System.out.println(s);
        System.out.println(s1);


        String s2 = empty.orElseGet(() -> "orElseGet");
        System.out.println(s2);

        String s3 = empty.orElseThrow(NullPointerException::new);
        System.out.println(s3);

    }
}
