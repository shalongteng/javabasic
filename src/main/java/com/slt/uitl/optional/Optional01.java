package com.slt.uitl.optional;

import java.util.Optional;

/**
 * of,empty,ofNullable
 *
 */
public class Optional01 {
    public static void main(String[] args) {
        //of 方法不能传递出null值做参数
//        Optional.of(null);
        Optional<Integer> optionalInteger = Optional.of(124);
        System.out.println(optionalInteger.get());

        Optional<Object> empty = Optional.empty();
        //如果value 为null get抛出NoSuchElementException
//        System.out.println(empty.get());

        Optional<String> optional = Optional.ofNullable("abcD");
        System.out.println(optional.get());


    }

    /**
     * optional 不要做参数，类的属性
     * 做返回值，她没有实现序列化接口。
     * @param optional
     */
    public static void test(Optional optional){

    }
}
