package com.slt.uitl.optional;

import java.util.Optional;

/**
 * 推荐函数式的写法
 */
public class Optional05 {
    public static void main(String[] args) {
        Optional<String> test = Optional.ofNullable("abcD");

        //optinonal 推荐使用方式
        test.ifPresent(System.out::println);

        //对象里 有个集合的属性
//        test.map(company->company.getEmployee()).orElse(collections.emptyList());
        test.map(String::toUpperCase).orElse(new String());

    }
}
