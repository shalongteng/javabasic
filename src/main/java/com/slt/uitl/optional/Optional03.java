package com.slt.uitl.optional;

import java.util.Optional;

/**
 * filter,map,flatmap
 */
public class Optional03 {
    public static void main(String[] args) {
        Optional<String> test = Optional.ofNullable("abcD");
        Optional<String> optional = test.filter(a -> a.startsWith("a"));
        System.out.println(optional.get());


        Optional<String> optional1 = optional.map(a -> a.toUpperCase());
        //打印结果 ABCD
        System.out.println(optional1.get());
        optional.ifPresent(System.out::println);

        Optional<String> optional2 = optional.flatMap(a -> Optional.of(a.toUpperCase()));
        System.out.println(optional2.get());

    }
}
