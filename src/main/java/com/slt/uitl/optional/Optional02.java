package com.slt.uitl.optional;

import java.util.Optional;

/**
 * isPresent,ifPresent
 */
public class Optional02 {
    public static void main(String[] args) {
        Optional<String> optional = Optional.ofNullable("abcD");
        Optional<Object> empty = Optional.empty();
        System.out.println(optional.isPresent());
        System.out.println(empty.isPresent());

        optional.ifPresent(s-> System.out.println(s.toUpperCase()));

    }
}
