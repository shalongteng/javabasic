package com.slt.dsa.collections.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *  * <PRE>
 *  *                      Element(0)   Element(1)   Element(2)   ... Element(n-1)
 *  * cursor positions:  ^            ^            ^            ^                  ^
 *  * </PRE>
 */
public class TesAbstracttList {
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);

        Iterator iterator = list.iterator();
        while (iterator.hasNext()){
            iterator.next();
            //remove 一次 lastRet 就会置为 -1，只有下次调用 next方法时候才能将lastRet置为 cursor
            iterator.remove();
        }
        System.out.println(list.size());

    }
}
