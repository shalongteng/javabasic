package com.slt.dsa.collections.list;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: shalongteng
 * @Date: 2021-09-05 19:31
 */
public class TestArrayList<E> {
    public static void main(String[] args) {
        List list = new ArrayList();
        //可以存储null元素
        list.add(null);
        list.add(null);
        list.add("b");
        list.add("c");

        System.out.println(list.size());
        System.out.println(list);
    }

    /**
     * java 不允许有泛型数组
     * https://blog.csdn.net/weixin_43821874/article/details/92793059
     */
    @Test
    public void testGener(){
//        E [] a = new E[4];
        int [] b = new int [4];
    }

    @Test
    public void testCon(){
        ArrayList<Object> list = new ArrayList<>();
        System.out.println(list.size());
    }
}
