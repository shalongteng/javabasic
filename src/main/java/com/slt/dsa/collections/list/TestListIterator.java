package com.slt.dsa.collections.list;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *  * <PRE>
 *  *                      Element(0)   Element(1)   Element(2)   ... Element(n-1)
 *  * cursor positions:  ^            ^            ^            ^                  ^
 *  * </PRE>
 */
public class TestListIterator {
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add("a");
        list.add("b");
        list.add("c");

        ListIterator listIterator = list.listIterator(0);
        System.out.println(listIterator.nextIndex());
        System.out.println(listIterator.previousIndex());
        System.out.println(listIterator.next());
        System.out.println(listIterator.nextIndex());
        System.out.println(listIterator.previousIndex());


        System.out.println(listIterator.next());
        System.out.println(listIterator.previous());
        listIterator.add(0);
        System.out.println(list);
    }
}
