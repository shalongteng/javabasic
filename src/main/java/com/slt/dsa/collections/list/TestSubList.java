package com.slt.dsa.collections.list;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: shalongteng
 * @Date: 2021-09-05 19:31
 */
public class TestSubList {
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add("a");
        list.add("b");
        list.add("c");

        List list1 = list.subList(1, 3);
        list1.add("d");
        System.out.println(list1);
        System.out.println(list);
    }
}
