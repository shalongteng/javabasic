package com.slt.dsa.collections.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * 集合排序
 *  retainAll 求交集
 *  把原集合中交集留下，其他删除
 */
public class Demo04 {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList1 = new ArrayList<>();
        arrayList1.add(1);
        arrayList1.add(2);
        arrayList1.add(5);
        ArrayList<Integer> arrayList2 = new ArrayList<>();
        arrayList2.add(2);
        arrayList2.add(3);
        arrayList2.add(4);

        arrayList1.retainAll(arrayList2);
        System.out.println(arrayList1);

    }
}
