package com.slt.java8.zhanglong.stream.collector;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@ToString
@Data
@AllArgsConstructor
public class User {
    private String name;
    private int score;
    private int age;

}
