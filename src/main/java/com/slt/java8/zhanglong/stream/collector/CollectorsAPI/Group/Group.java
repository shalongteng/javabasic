package com.slt.java8.zhanglong.stream.collector.CollectorsAPI.Group;

import com.slt.java8.zhanglong.stream.collector.User;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 分组，分区
 */
public class Group {
    public static void main(String[] args) {
        List<User> userList = initUser();

        //分组：按照分数（返回的map的key是score,value是stream中的元素）
        Map<Integer, List<User>> scoreUsers = userList.stream().collect(Collectors.groupingBy(User::getScore));
        System.out.println("scoreUsers = " + scoreUsers);

        //二级分组：先按照分数分组，返回一个Map<Integer, List<User>>, 再根据用户名分组
        Map<Integer, Map<String, List<User>>> scoreNameUsers = userList.stream().collect(Collectors.groupingBy(User::getScore, Collectors.groupingBy(User::getName)));
        System.out.println("scoreNameUsers = " + scoreNameUsers);

        //分区,是否及格
        Map<Boolean, List<User>> jigeUsers = userList.stream().collect(Collectors.partitioningBy(user -> user.getScore() >= 60));
        System.out.println("jigeUsers = " + jigeUsers);

        //二级分区,是否及格,及格里面是否大于80
        Map<Boolean, Map<Boolean, List<User>>> youxiuUsers = userList.stream().collect(Collectors.partitioningBy(user -> user.getScore() >= 60, Collectors.partitioningBy(user -> user.getScore() >= 80)));
        System.out.println("youxiuUsers = " + youxiuUsers);

        //分区,是否及格,算出及格的个数
        Map<Boolean, Long> jigeUserCount = userList.stream().collect(Collectors.partitioningBy(user -> user.getScore() >= 60, Collectors.counting()));
        System.out.println("jigeUserCount = " + jigeUserCount);

        /**
         * 先按照名字分组,获取每个分组分数最小的
         * collectingAndThen
         *  先收集 然后做一个操作
         */
        Map<String, Optional<User>> collect = userList.stream().collect(Collectors.groupingBy(User::getName, Collectors.minBy((a, b) -> a.getScore() - b.getScore())));
        Map<String, User> UserCount = userList.stream().collect(Collectors.groupingBy(User::getName, Collectors.collectingAndThen(Collectors.minBy(Comparator.comparingInt(User::getScore)), Optional::get)));
        System.out.println("UserCount = " + UserCount);
        System.out.println("UserCount = " + collect.get("shalongteng").get());

    }

    public static List<User> initUser(){
        User user1 = new User("zhangsan", 60, 20);
        User user2 = new User("lisi", 60, 20);
        User user4 = new User("wangwu", 80, 24);
        User user3 = new User("zhaoliu", 80, 24);
        User user5 = new User("shalongteng", 90, 24);

        List<User> userList = Arrays.asList(user1, user2, user3, user4, user5);
        return userList;
    }
}
