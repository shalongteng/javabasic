package com.slt.java8.zhanglong.stream.createStream;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * 创建流的几种方式
 * Stream.of
 */
public class CreateStream01 {
    public static void main(String[] args) {
        //缺省参数和数组等同
        Stream<String> stream = Stream.of("hello", "world", "hello world");
        String[] myArray = new String[]{"hello", "world", "hello world"};
        Stream stream2 = Stream.of(myArray);

        //根据数组创建流
        Stream stream3 = Arrays.stream(myArray);

        //Stream.of   一个参数
        Stream<String> stream1 = Stream.of("hello");

        stream1.forEach(System.out::println);
        System.out.println("-------------------------");
        stream.forEach(System.out::println);
        System.out.println("-------------------------");
        stream2.forEach(System.out::println);
    }
}
