package com.slt.java8.zhanglong.stream.collector.CollectorsAPI.ReduceAndSum;

import com.slt.java8.zhanglong.stream.collector.User;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 算出分数最小的那个并输出
 * 算出分数最大的那个并输出（无法做到多个并列的时候求值）
 */
public class MinMax {
    public static void main(String[] args) {
        List<User> userList = initUser();

        //方法一 直接使用stream的 min方法
        System.out.println("方法一 直接使用stream的 min方法");
        userList.stream().min(Comparator.comparingInt(item->item.getScore())).ifPresent(System.out::println);
        System.out.println(userList.stream().min((a, b) -> a.getScore() - b.getScore()).get());

        //方法二 使用stream的 reduce方法自己实现 min
        System.out.println("方法二 直接使用stream的 min方法");
        userList.stream().mapToInt(User::getScore).reduce((a,b)-> (a <= b) ? a : b).ifPresent(System.out::println);
        userList.stream().mapToInt(User::getScore).reduce(Integer::min).ifPresent(System.out::println);

        //方法三 使用collector 自己实现comparator比较器
        System.out.println("方法三 使用collector 自己实现comparator比较器");
        userList.stream().collect(Collectors.minBy(Comparator.comparingInt(User::getScore))).ifPresent(System.out::println);
        userList.stream().collect(Collectors.minBy((a,b)->a.getScore() - b.getScore())).ifPresent(System.out::println);

        /**
         * map 返回的是 stream<Integer> 所以minBy中 可以推断出来是 int类型
         * maptoInt 返回的是一个intStream，intstream中collect是接受三个参数的
         */
        userList.stream().map(User::getScore).collect(Collectors.minBy((a,b)->a-b)).ifPresent(System.out::println);
//        userList.stream().mapToInt(User::getScore).collect(Collectors.minBy((a,b)->a-b)).ifPresent(System.out::println);

    }

    public static List<User> initUser(){
        User user1 = new User("zhangsan", 50, 20);
        User user2 = new User("lisi", 60, 20);
        User user4 = new User("wangwu", 70, 24);
        User user3 = new User("zhaoliu", 80, 24);
        User user5 = new User("shalongteng", 90, 24);

        List<User> userList = Arrays.asList(user1, user2, user3, user4, user5);
        return userList;
    }
}
