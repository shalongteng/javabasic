package com.slt.java8.zhanglong.stream.IntStream;

import java.util.stream.IntStream;

/**
 * intstream 创建
 *  of
 *  range
 *  rangeClosed
 */
public class IntStream01 {
    public static void main(String[] args) {
        IntStream.of(new int[]{3, 4, 5, 6, 7}).forEach(System.out::print);
        System.out.println();

        IntStream.range(3, 8).forEach(System.out::print);
        System.out.println();

        IntStream.rangeClosed(3, 7).forEach(System.out::print);
        System.out.println();

        System.out.println("------------------------");
    }
}
