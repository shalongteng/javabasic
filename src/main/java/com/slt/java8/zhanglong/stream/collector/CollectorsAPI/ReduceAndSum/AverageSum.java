package com.slt.java8.zhanglong.stream.collector.CollectorsAPI.ReduceAndSum;

import com.slt.java8.zhanglong.stream.collector.User;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 算出分数平均值并输出
 */
public class AverageSum {
    public static void main(String[] args) {
        List<User> userList = initUser();
        /**
         * 算出分数平均值并输出
         */
        Double averagint = userList.stream().collect(Collectors.averagingDouble(User::getScore));
        System.out.println("averagint = " + averagint);


        /**
         * 算出分数总和并输出
         */
        //方法一 使用collectors.summingInt
        int summingInt = userList.stream().collect(Collectors.summingInt(User::getScore));
        System.out.println("summingInt = " + summingInt);
        //方法二 使用stream的reduce方法
        userList.stream().map(User::getScore).reduce((a,b)-> a+b).ifPresent(System.out::println);
        /**
         * reduce 归约
         *  参数是一个 BinaryOperator，参数类型和返回类型需要一致。
         */
//        userList.stream().reduce((a,b)->a.getScore() + b.getScore());


        /**
         * 算出汇总信息
         */
        IntSummaryStatistics intSummaryStatistics = userList.stream().collect(Collectors.summarizingInt(User::getScore));
        System.out.println("intSummaryStatistics = " + intSummaryStatistics);

    }

    public static List<User> initUser(){
        User user1 = new User("zhangsan", 50, 20);
        User user2 = new User("lisi", 60, 20);
        User user4 = new User("wangwu", 70, 24);
        User user3 = new User("zhaoliu", 80, 24);
        User user5 = new User("shalongteng", 90, 24);

        List<User> userList = Arrays.asList(user1, user2, user3, user4, user5);
        return userList;
    }
}
