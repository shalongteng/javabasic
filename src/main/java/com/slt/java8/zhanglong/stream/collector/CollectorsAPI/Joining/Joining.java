package com.slt.java8.zhanglong.stream.collector.CollectorsAPI.Joining;

import com.slt.java8.zhanglong.stream.collector.User;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * collectors 对string的 支持
 */
public class Joining {
    public static void main(String[] args) {
        List<User> userList = initUser();

        /**
         * 拼接名字
         */
        String nameStrs = userList.stream().map(User::getName).collect(Collectors.joining(", "));
        System.out.println("nameStrs = " + nameStrs);

        //拼接名字，调用另外一个方法，可以加前缀和后缀
        String nameStrs2 = userList.stream().map(User::getName).collect(Collectors.joining(", ", "[", "]"));
        System.out.println("nameStrs2 = " + nameStrs2);


    }

    public static List<User> initUser(){
        User user1 = new User("zhangsan", 50, 20);
        User user2 = new User("lisi", 60, 20);
        User user4 = new User("wangwu", 70, 24);
        User user3 = new User("zhaoliu", 80, 24);
        User user5 = new User("shalongteng", 90, 24);

        List<User> userList = Arrays.asList(user1, user2, user3, user4, user5);
        return userList;
    }
}
