package com.slt.java8.zhanglong.stream;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

/**
 * @Author: shalongteng
 * @Date: 2021-06-29 21:25
 */
public class TestJoining {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("hello", "world", "sha");
        String collect = list.stream().collect(Collectors.joining(", "));
        System.out.println(collect);


        StringJoiner stringJoiner = new StringJoiner(", ", "", "");
        stringJoiner.add("hello").add("world");
        System.out.println(stringJoiner);


        list.stream()
                .map(item->item)
                .filter(item->item.equals("hello"))
        .forEach(System.out::println);
    }
}
