package com.slt.java8.zhanglong.stream.spliterator;

import java.util.ArrayList;
import java.util.Spliterator;

/**
 * @Author: shalongteng
 * @Date: 2021-07-04 17:04
 */
public class Spliterator01 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");

        Spliterator<String> s0 = list.spliterator();

        Spliterator<String> s1 = s0.trySplit();
        Spliterator<String> s2 = s0.trySplit();

        System.out.println("s1.consume");
        s1.forEachRemaining(System.out::println);

        System.out.println("s2.consume");
        s2.forEachRemaining(System.out::println);

        System.out.println("s0.consume :");
        s0.forEachRemaining(item-> System.out.println(item));
    }
}
