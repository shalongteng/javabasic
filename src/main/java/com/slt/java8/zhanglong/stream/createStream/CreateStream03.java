package com.slt.java8.zhanglong.stream.createStream;

import java.util.UUID;
import java.util.stream.Stream;

/**
 * 无限流生成
 * generate方法使用
 */
public class CreateStream03 {
    public static void main(String[] args) {
        Stream<String> stream = Stream.generate(UUID.randomUUID()::toString);
        stream.limit(5).forEach(System.out::println);
    }
}
