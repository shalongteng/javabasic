package com.slt.java8.zhanglong.stream.createStream;

import java.util.stream.Stream;

/**
 * 无限流生成
 * Stream.iterate
 * 一般来说，在需要依次生成一系列值的时候应该使用iterate。
 */
public class CreateStream02 {
    public static void main(String[] args) {
        //iterate方法使用
        Stream.iterate(1, item -> item + 2).limit(5).forEach(System.out::println);

    }
}
