package com.slt.java8.datetime.exercise;

import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Exercise01 {
    public static void main(String[] args) {

    }

    /**
     * LocalDateTime 格式化
     * @param pattern
     * @param localDateTime
     * @return
     */
    public String localDateTimeFormat(String pattern,LocalDateTime localDateTime){
        return DateTimeFormatter.ofPattern(pattern).format(localDateTime);
    }
    /**
     * 获取当前的时间
     */
    @Test
    public void testCurrentDate(){
        System.out.println("当前时间是:\n" + new Date());

        System.out.println(localDateTimeFormat("yyyy-MM-dd HH:mm:ss",LocalDateTime.now()));
    }

    /**
     * 字符串"2017-08-16"转换为对应的java.util.Date类的对象。
     */
    @Test
    public void string2Date() throws ParseException {
        String str = "2017-08-16";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = dateFormat.parse(str);
        System.out.println(parse);


        LocalDate localDate = LocalDate.parse(str, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        System.out.println(localDate);
    }

    /**
     * 将Date类型转换成String类型
     */
    @Test
    public void date2String() throws ParseException {
        String pattern = "yyyy-MM-dd";
        System.out.println(new SimpleDateFormat(pattern).format(new Date()));


        System.out.println(LocalDate.now().format(DateTimeFormatter.ofPattern(pattern)));
    }
}
