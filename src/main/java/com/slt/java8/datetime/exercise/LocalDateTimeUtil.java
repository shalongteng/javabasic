package com.slt.java8.datetime.exercise;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.WeekFields;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * @Description: java8之前 日期时间工具类
 * @Author: shalongteng
 * @Date: 2021/8/4 14:33
 **/
public class LocalDateTimeUtil {
    //数据库格式的日期
    public static final String SQL_MONTH = "yyyy-MM";
    public static final String SQL_DATE = "yyyy-MM-dd";
    public static final String SQL_TIME = "yyyy-MM-dd HH:mm:ss";
    public static final String SQL_TIMESTAMP = "yyyy-MM-dd HH:mm:ss.SS";

    //斜杠格式的日期
    public static final String DATE = "yyyy/MM/dd";
    public static final String TIMESTAMP = "yyyy/MM/dd HH:mm:ss.SS";
    public static final String TIMESTAMP_SHORT = "yyyy/MM/dd HH:mm";
    public static final String TIME = "HH:mm:ss";
    public static final String TIME_SHORT = "HH:mm";

    //不常用日期格式
    public static final String CHINESEDATE = "yyyy年MM月dd日";
    public static final String DATE_TIME = "yyyyMMddHHmmss";
    public static final String DATE_TIME_DETAIL = "yyyyMMddHHmmssSS";
    public static final String DATE_DAY = "yyyyMMdd";
    public static final String DATE_HOUR = "yyyyMMddHH";

    /**
     * 获取当前的时间
     *
     * @return java.sql.Timestamp
     */
    public static LocalDateTime getCurrontTime() {
        return LocalDateTime.now();
    }


    /**
     * Date转LocalDateTime
     * 使用系统时区
     * @param date
     * @return
     */
    public static LocalDateTime dateToLocalDateTime(Date date){
        Instant instant = date.toInstant();
        ZoneId zoneId = ZoneId.systemDefault();
        LocalDateTime localDateTime = instant.atZone(zoneId).toLocalDateTime();
        return localDateTime;
    }


    /**
     * LocalDateTime转Date
     * 使用系统时区
     * @param localDateTime
     * @return
     */
    public static Date localDateTimeToDate(LocalDateTime localDateTime){
        ZonedDateTime zdt = localDateTime.atZone(ZoneId.systemDefault());
        return Date.from(zdt.toInstant());
    }
    /**
     * 将Date类型转换成String类型
     *
     * @param localDateTime Date对象
     * @return 形如:"yyyy-MM-dd HH:mm:ss"
     */
    public static String dateTime2String(LocalDateTime localDateTime) {
        return dateTime2String(localDateTime, SQL_TIME);
    }

    /**
     * 把date 按照 pattern形式转成 string
     *
     * @return
     */
    public static String dateTime2String(LocalDateTime localDateTime, String pattern) {
        if (null == localDateTime || null == pattern) {
            return null;
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
//        localDateTime.format(dateTimeFormatter);
        return dateTimeFormatter.format(localDateTime);
    }


    /**
     * 将String类型转换成Date类型
     * 默认按照
     *
     * @return
     */
    public static LocalDateTime string2DateTime(String date) {
        return string2DateTime(date, SQL_TIME);
    }

    /**
     * 将String类型转换成Date类型
     *
     * @return
     */
    public static LocalDateTime string2DateTime(String date, String pattern) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        try {
            return LocalDateTime.parse(date, dateTimeFormatter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static LocalDateTime plusOneDay(int day) {
        return plusDays(LocalDateTime.now(), 1);
    }

    /**
     * @Description: 获取当前日期 N天后的日期
     * @Author: shalongteng
     * @Date: 2021/8/4 14:15
     **/
    public static LocalDateTime plusDays(int day) {
        return plusDays(LocalDateTime.now(), day);
    }

    /**
     * 获取某日期N天后的日期
     * Calendar 时间计算
     *
     * @param day
     * @return
     */
    public static LocalDateTime plusDays(LocalDateTime localDateTime, int day) {
        return localDateTime.plusDays(day);
    }



    public static LocalDateTime plusOneMonth(int month) {
        return plusMonths(LocalDateTime.now(), 1);
    }

    public static LocalDateTime plusMonths(int month) {
        return plusMonths(LocalDateTime.now(), month);
    }

    public static LocalDateTime plusMonths(LocalDateTime localDateTime, int month) {
        return localDateTime.plusMonths(month);
    }

    /**
     * @Description: 获取当前日期的前一天
     * @author: shalongteng
     */
    public static LocalDateTime minusOneDay() {
        return minusDays(1);
    }

    public static LocalDateTime minusDays(int day) {
        return minusDays(LocalDateTime.now(), day);
    }

    public static LocalDateTime minusDays(LocalDateTime localDateTime, int day) {
        return localDateTime.minusDays(day);
    }

    /**
     * 获得当前年的第一天
     * @param
     * @return
     */
    public static LocalDateTime getYearFirstDay(LocalDateTime localDateTime) {
        return localDateTime.with(TemporalAdjusters.firstDayOfYear());
    }

    /**
     * 获得当前年的最后一天
     * @param
     * @return
     */
    public static LocalDateTime getYearLastDay(LocalDateTime localDateTime) {
        return localDateTime.with(TemporalAdjusters.lastDayOfYear());
    }


    /**
     * 获得当前月的第一天
     * @param
     * @return
     */
    public static LocalDateTime getMonthFirstDay(LocalDateTime localDateTime) {
        return localDateTime.with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * 获得当前月的最后一天
     * @param
     * @return
     */
    public static LocalDateTime getMonthLastDay(LocalDateTime localDateTime) {
        return localDateTime.with(TemporalAdjusters.lastDayOfMonth());
    }


    /**
     * 获得当前星期的第一天
     * @param localDateTime
     * @param locale 默认Locale.CHINA 周日为一周的第一天
     * @return
     */
    public static LocalDateTime getWeekFirstDay(LocalDateTime localDateTime, Locale locale) {
        return localDateTime.with(WeekFields.of(locale==null?Locale.CHINA:locale).dayOfWeek(),1);
    }

    /**
     * 获得当前星期的最后一天
     * @param localDateTime
     * @param locale 默认默认Locale.CHINA 周日为一周的第一天
     * @return
     */
    public static LocalDateTime getWeekLastDay(LocalDateTime localDateTime, Locale locale) {
        return localDateTime.with(WeekFields.of(locale==null?Locale.CHINA:locale).dayOfWeek(),7);
    }



    /**
     * 计算两个日期之间相差年数
     * @param smallDateTime 较小的时间
     * @param bigDateTime  较大的时间
     * @return 相差年数
     */
    public static int getYearDiff(LocalDateTime smallDateTime, LocalDateTime bigDateTime) {
        return (int)smallDateTime.until(bigDateTime, ChronoUnit.YEARS);
    }

    /**
     * 计算两个日期之间相差月数
     * @param smallDateTime 较小的时间
     * @param bigDateTime  较大的时间
     * @return 相差月数
     */
    public static int getMonthDiff(LocalDateTime smallDateTime, LocalDateTime bigDateTime) {
        return (int)smallDateTime.until(bigDateTime, ChronoUnit.MONTHS);
    }

    /**
     * 计算两个日期之间相差的天数
     * @param smallDateTime 较小的时间
     * @param bigDateTime  较大的时间
     * @return 相差天数
     */
    public static int getDayDiff(LocalDateTime smallDateTime, LocalDateTime bigDateTime){
        return (int)smallDateTime.until(bigDateTime, ChronoUnit.DAYS);
    }

    /**
     * 计算两个日期之间相差小时数
     * @param smallDateTime 较小的时间
     * @param bigDateTime  较大的时间
     * @return 相差小时数
     */
    public static int getHourDiff(LocalDateTime smallDateTime, LocalDateTime bigDateTime){
        return (int)smallDateTime.until(bigDateTime, ChronoUnit.HOURS);
    }

    /**
     * 计算两个日期之间相差分钟数
     * @param smallDateTime
     * @param bigDateTime
     * @return 相差分钟数
     */
    public static int getMinutesDiff(LocalDateTime smallDateTime, LocalDateTime bigDateTime){
        return (int)smallDateTime.until(bigDateTime, ChronoUnit.MINUTES);
    }

    /**
     * 计算两个日期之间相差秒数
     * @param smallDateTime
     * @param bigDateTime
     * @return 相差秒数
     */
    public static int getSecondsDiff(LocalDateTime smallDateTime, LocalDateTime bigDateTime){
        return (int)smallDateTime.until(bigDateTime, ChronoUnit.SECONDS);
    }

}
