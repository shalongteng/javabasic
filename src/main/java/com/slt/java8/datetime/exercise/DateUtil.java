package com.slt.java8.datetime.exercise;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * @Description: java8之前 日期时间工具类
 * @Author: shalongteng
 * @Date: 2021/8/4 14:33
 **/
public class DateUtil {
    //数据库格式的日期
    public static final String SQL_MONTH = "yyyy-MM";
    public static final String SQL_DATE = "yyyy-MM-dd";
    public static final String SQL_TIME = "yyyy-MM-dd HH:mm:ss";
    public static final String SQL_TIMESTAMP = "yyyy-MM-dd HH:mm:ss.SS";

    //斜杠格式的日期
    public static final String DATE = "yyyy/MM/dd";
    public static final String TIMESTAMP = "yyyy/MM/dd HH:mm:ss.SS";
    public static final String TIMESTAMP_SHORT = "yyyy/MM/dd HH:mm";
    public static final String TIME = "HH:mm:ss";
    public static final String TIME_SHORT = "HH:mm";

    //不常用日期格式
    public static final String CHINESEDATE = "yyyy年MM月dd日";
    public static final String DATE_TIME = "yyyyMMddHHmmss";
    public static final String DATE_TIME_DETAIL = "yyyyMMddHHmmssSS";
    public static final String DATE_DAY = "yyyyMMdd";
    public static final String DATE_HOUR = "yyyyMMddHH";

    /**
     * 获取当前的时间
     *
     * @return java.sql.Timestamp
     */
    public static Timestamp getCurrontTime() {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * 将Date类型转换成String类型
     *
     * @param date Date对象
     * @return 形如:"yyyy-MM-dd HH:mm:ss"
     */
    public static String date2String(Date date) {
        return date2String(date, SQL_TIME);
    }

    /**
     * 把date 按照 pattern形式转成 string
     *
     * @return
     */
    public static String date2String(Date date, String pattern) {
        if (null == date || null == pattern) {
            return null;
        }
        return new SimpleDateFormat(pattern).format(date);
    }


    /**
     * 将String类型转换成Date类型
     * 默认按照
     *
     * @return
     */
    public static Date string2Date(String date) {
        return string2Date(date, SQL_TIME);
    }

    /**
     * 将String类型转换成Date类型
     *
     * @return
     */
    public static Date string2Date(String date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            return format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Timestamp string2Timestamp(String date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            Date date1 = format.parse(date);
            return new Timestamp(date1.getTime());
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return null;
    }

    public static Date plusOneDay(int day) {
        return plusDays(new Date(), 1);
    }

    /**
     * @Description: 获取当前日期 N天后的日期
     * @Author: shalongteng
     * @Date: 2021/8/4 14:15
     **/
    public static Date plusDays(int day) {
        return plusDays(new Date(), day);
    }

    /**
     * 获取某日期N天后的日期
     * Calendar 时间计算
     *
     * @param day
     * @return
     */
    public static Date plusDays(Date date, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, day);

        return cal.getTime();
    }

    public static Date plusOneMonth(int month) {
        return plusMonths(new Date(), 1);
    }

    public static Date plusMonths(int month) {
        return plusMonths(new Date(), month);
    }

    public static Date plusMonths(Date date, int month) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, month);

        return cal.getTime();
    }

    /**
     * @Description: 获取当前日期的前一天
     * @author: shalongteng
     */
    public static Date minusOneDay() {
        return minusDays(1);
    }

    public static Date minusDays(int day) {
        return minusDays(new Date(), day);
    }

    public static Date minusDays(Date date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return calendar.getTime();
    }

    /**
     * @Description: 获取今天是星期几
     * @Author: shalongteng
     * @Date: 2021/8/4 14:51
     **/
    public static String getDayOfWeek() {
        return getDayOfWeek(new Date());
    }

    /**
     * 获取指定日期 是星期几
     *
     * @return
     */
    public static String getDayOfWeek(Date date) {
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0) {
            w = 0;
        }
        return weekDays[w];
    }

    /**
     * @Description: 两个时间比较大小
     * @Author: shalongteng
     * @Date: 15:02
     **/
    public static int compareDate(Date date1, Date date2) {
        if (date1.getTime() > date2.getTime()) {
            System.out.println("dt1 在dt2前");
            return 1;
        } else if (date1.getTime() < date2.getTime()) {
            System.out.println("dt1在dt2后");
            return -1;
        } else {
            return 0;
        }
    }


    /**
     * 返回当前时间的毫秒数
     *
     * @return
     */
    public static Long getTimeMillions() {
        return new Date().getTime();
    }

    /**
     * @Description: 获取本月第一天
     * @Author: shalongteng
     * @Date: 2021/8/4 15:23
     **/
    public static Date getFirstDayOfMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        return calendar.getTime();
    }

    public static Date getLastDayOfMonth() {
        Calendar calendar = Calendar.getInstance();
        int maxday = calendar.getActualMaximum(Calendar.DATE);
        calendar.set(Calendar.DAY_OF_MONTH, maxday);

        return calendar.getTime();
    }

    /**
     * @Description: 计算date2 - date1之间相差的天数
     * @Author: shalongteng
     * @Date: 2021/8/5 16:33
     **/
    public static int daysBetween(Date date1, Date date2) {
        long time1 = date1.getTime();
        long time2 = date2.getTime();
        return Integer.parseInt(String.valueOf((time2 - time1) / 86400000L));
    }
}
