package com.slt.java8.datetime.java7;

import com.slt.java8.datetime.exercise.DateUtil;

import java.time.ZoneId;
import java.util.Arrays;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * 获取timezone实例
 */
public class TestTimeZone01 {
    public static void main(String[] args) {
        TimeZone timeZone = TimeZone.getDefault();
        System.out.println(timeZone);

        TimeZone timeZone1 = TimeZone.getTimeZone("America/Los_Angeles");
        System.out.println(timeZone1);


        TimeZone timeZone2 = TimeZone.getTimeZone("GMT+:08:00");
        TimeZone timeZone3 = TimeZone.getTimeZone(ZoneId.of("Asia/Tokyo"));
        System.out.println(timeZone2);
        System.out.println(timeZone3);


        //获取Java支持的所有时区ID
//        System.out.println(Arrays.toString(TimeZone.getAvailableIDs()));
    }
}
