package com.slt.java8.datetime.java7;

import com.slt.java8.datetime.exercise.DateUtil;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * java.util.Date
 * tostring
 */
public class TestDate03 {
    public static void main(String[] args) {
        Date date = new Date();
        System.out.println(date.toString());
        //内部调用了 dateformatter
        System.out.println(date.toLocaleString());
        //GMT 0时区的值
        System.out.println(date.toGMTString());
    }
}
