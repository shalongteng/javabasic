package com.slt.java8.datetime.java7;

import com.slt.java8.datetime.exercise.DateUtil;

import java.util.Calendar;
import java.util.Date;

/**
 * java.util.Date
 * 跟date转换
 */
public class TestCalendar03 {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        Date time = calendar.getTime();
        System.out.println(DateUtil.date2String(time));
        /**
         * 获取某个属性最大 最小值
         */
        System.out.println(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        System.out.println(calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

        System.out.println(calendar.getFirstDayOfWeek());
        System.out.println(calendar.getTimeZone());
        System.out.println(calendar.getCalendarType());
    }
}
