package com.slt.java8.datetime.java7;

import com.slt.java8.datetime.exercise.DateUtil;

import java.util.Calendar;

/**
 * java.util.Date
 * SET GET 方法
 */
public class TestCalendar01 {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        /**
         * get 方法
         * 根据calendar属性值  获取不同的值
         */
        System.out.println(DateUtil.date2String(calendar.getTime()));
        System.out.println(calendar.get(Calendar.YEAR));
        System.out.println(calendar.get(Calendar.MONTH));
        System.out.println(calendar.get(Calendar.DAY_OF_WEEK));
        System.out.println(calendar.get(Calendar.DAY_OF_MONTH));
        System.out.println(calendar.get(Calendar.DAY_OF_YEAR));


        /**
         * set方法
         */
        calendar.set(Calendar.YEAR,2022);
        calendar.set(Calendar.MONTH,8);
        calendar.set(Calendar.DATE,2);
        System.out.println(DateUtil.date2String(calendar.getTime()));
    }
}
