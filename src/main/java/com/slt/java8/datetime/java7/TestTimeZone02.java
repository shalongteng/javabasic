package com.slt.java8.datetime.java7;

import java.util.Arrays;
import java.util.TimeZone;

/**
 * 获取timezone实例
 */
public class TestTimeZone02 {
    public static void main(String[] args) {
        TimeZone timeZone = TimeZone.getDefault();
        //将TimeZone对象转换为ZoneId对象
        System.out.println(timeZone.toZoneId());
        //
        System.out.println(timeZone.getID());
        System.out.println(timeZone.getDisplayName());


    }
}
