package com.slt.java8.datetime.java7;

import com.slt.java8.datetime.exercise.DateUtil;

import java.util.Date;

/**
 * java.util.Date
 * 构造器
 */
public class TestDate02 {
    public static void main(String[] args) {
        Date date = new Date();
        /**
         * 当前时间 是否比给定时间 更靠后
         */
        System.out.println(date.after(DateUtil.minusDays(1)));
        /**
         * equals 比较是两个date的getTime()
         */
        System.out.println(date.equals(new Date()));

        date.setYear(100);
        System.out.println(date);
        System.out.println(date.getDay());


        System.out.println(date.getTime());

    }
}
