package com.slt.java8.datetime.java7;

import com.slt.java8.datetime.exercise.DateUtil;

import java.time.Instant;
import java.util.Date;

/**
 * java.util.Date
 * 1.8新加的方法
 */
public class TestDate04 {
    public static void main(String[] args) {
        /**
         * 从instant 转 date
         */
        System.out.println(Date.from(Instant.now()));


        System.out.println(new Date().toInstant());
    }
}
