package com.slt.java8.datetime.java7;

import com.slt.java8.datetime.exercise.DateUtil;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * java.util.Date
 * 构造器
 */
public class TestDate01 {
    public static void main(String[] args) {
        /**
         * Tue Aug 03 09:59:01 CST 2021
         * CST： China Standard Time UT+8:00
         */
        System.out.println(new Date());

        //此处需要使用1000L,此处L不能往最后加，前边已经溢出了
        //int值最大到1970-01-07 15:56:42
        System.out.println(DateUtil.date2String(new Date(1000L*86400*365*40)));


        /**
         * year + 1900  120+1900=2020
         * month 0-11   7+1=8
         * 输出的是 8点
         * 废弃的方法
         * 推荐使用calendar.set方法
         */
        System.out.println(DateUtil.date2String(new Date(121,-2,23)));

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(2021,7,23);
        System.out.println(DateUtil.date2String(gregorianCalendar.getTime()));


        System.out.println(new Date("Sat, 12 Aug 1995 13:30:00 GMT+0430"));
    }
}
