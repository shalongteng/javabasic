package com.slt.java8.datetime.java7;

import com.slt.java8.datetime.exercise.DateUtil;

import java.util.Calendar;

/**
 * java.util.Date
 * 计算 add
 */
public class TestCalendar02 {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR,2);
        calendar.add(Calendar.MONTH,2);
        System.out.println(DateUtil.date2String(calendar.getTime()));
    }
}
