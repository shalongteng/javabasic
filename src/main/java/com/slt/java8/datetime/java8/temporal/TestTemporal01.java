package com.slt.java8.datetime.java8.temporal;

import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.time.chrono.ChronoPeriod;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;

public class TestTemporal01 {
    public static void main(String[] args) {
        LocalDate localDate = LocalDate.of(2019, 1, 1);
        LocalDate endDate = LocalDate.of(2019, 1, 16);
        long days = localDate.until(endDate, ChronoUnit.DAYS);
        System.out.println(days);

        System.out.println(localDate.minus(10, ChronoUnit.DAYS));

        System.out.println(localDate.with(ChronoField.DAY_OF_YEAR,10));
        System.out.println(localDate.with(ChronoLocalDate.from(endDate)));

    }
}
