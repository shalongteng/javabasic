package com.slt.java8.datetime.java8.zoneid;

import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.Locale;

/**
 * @Author: shalongteng
 * @Date: 获取实例
 */
public class TestZoneId01 {
    public static void main(String[] args) {
        ZoneId zoneId = ZoneId.of("Asia/Shanghai");
        System.out.println(zoneId);

        ZoneId zoneId1 = ZoneId.systemDefault();
        System.out.println(zoneId1);

        System.out.println(zoneId.getId());
        System.out.println(zoneId.getDisplayName(TextStyle.FULL, Locale.ROOT));
    }
}
