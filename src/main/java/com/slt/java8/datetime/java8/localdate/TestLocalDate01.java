package com.slt.java8.datetime.java8.localdate;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;

/**
 * Java8时间LocalDate的使用
 */
public class TestLocalDate01 {
    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate.isSupported(ChronoField.DAY_OF_WEEK));
        System.out.println(localDate.get(ChronoField.DAY_OF_MONTH));
        System.out.println(localDate.getLong(ChronoField.DAY_OF_MONTH));
        System.out.println(localDate.range(ChronoField.DAY_OF_YEAR));
    }
}
