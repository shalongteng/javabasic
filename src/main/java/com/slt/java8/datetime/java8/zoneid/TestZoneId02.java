package com.slt.java8.datetime.java8.zoneid;

import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.Locale;

/**
 * @Author: shalongteng
 * @Date: 获取实例
 */
public class TestZoneId02 {
    public static void main(String[] args) {
        ZoneId zoneId = ZoneId.of("Asia/Shanghai");
        System.out.println(zoneId.normalized());
        System.out.println(zoneId.getRules());
        System.out.println(zoneId.getId());


    }
}
