package com.slt.java8.MyStream.stream;

import com.slt.java8.MyStream.stream.genetator.IntegerStreamGenerator;


/**
 * @Author: shalongteng
 * @Date: 2021-07-16 22:55
 */
public class TestMyStream {
    public static void main(String[] args) {
        MyStream<Integer> myStream = IntegerStreamGenerator.getIntegerStream(0, 10);
        myStream.forEach(item -> {
            System.out.println(item);
        });

        myStream.map(item->item+1).forEach(System.out::println);
    }
}
