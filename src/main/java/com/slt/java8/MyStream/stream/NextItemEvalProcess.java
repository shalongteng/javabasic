package com.slt.java8.MyStream.stream;

import com.slt.java8.MyStream.function.EvalFunction;

/**
 * @Author shalongteng
 * @Date 2021/6/6
 *
 * 下一个元素求值过程
 */
public class NextItemEvalProcess {

    /**
     * 求值函数
     * */
    private EvalFunction evalFunction;

    /**
     * constructor
     * @param evalFunction
     */
    public NextItemEvalProcess(EvalFunction evalFunction) {
        this.evalFunction = evalFunction;
    }
    /** 
     * 求值
     */ 
    MyStream eval(){
        return evalFunction.apply();
    }
}
