package com.slt.java8.java8InAction.function.function;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import static java.util.stream.Collectors.toMap;

/**
 * identity
 * https://www.jianshu.com/p/cd694d2d8be5
 */
public class Function03 {
    public static void main(String[] args) {
        Map<String, String> map = Arrays.asList("a", "b", "c")
                .stream()
                .map(Function.identity()) // <- This,
                .map(str -> str)          // <- is the same as this.
                .collect(toMap(
                        Function.identity(), // <-- And this,
                        str -> str));// <-- is the same as this.
        //此处 str -> str 和 identity效果等同
        System.out.println(map);
    }

    /**
     * Task::getTitle需要一个task并产生一个仅有一个标题的key。
     * task -> task是一个用来返回自己的lambda表达式，上例中返回一个task。
     */
    private static Map<String, Task> taskMap(List<Task> tasks) {
        return tasks.stream().collect(toMap(Task::getTitle, task -> task));
    }

    /**
     * 使用Function接口中的默认方法identity来让上面的代码代码变得更简洁明了、
     * 传递开发者意图时更加直接，下面是采用identity函数的代码。
     * @param tasks
     * @return
     */
    private static Map<String, Task> taskMap2(List<Task> tasks) {
        return tasks.stream().collect(toMap(Task::getTitle, Function.identity()));
    }

}
