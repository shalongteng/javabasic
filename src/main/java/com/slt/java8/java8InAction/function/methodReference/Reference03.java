package com.slt.java8.java8InAction.function.methodReference;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 构造方法引用
 */
public class Reference03 {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Bob", "Alice", "Tim");
        List<Person> persons = names.stream().map(Person::new).collect(Collectors.toList());
        System.out.println(persons);
    }




    static class Person {
        String name;
        public Person(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

}
