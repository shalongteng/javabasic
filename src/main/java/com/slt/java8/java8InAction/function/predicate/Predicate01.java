package com.slt.java8.java8InAction.function.predicate;


import com.slt.java8.java8InAction.common.Apple;

import java.util.ArrayList;
import java.util.List;

/**
 * 需求：筛选绿苹果
 */
public class Predicate01 {
    public static void main(String[] args) {

    }

    /**
     * 筛选绿苹果
     * @return
     */
    public static List<Apple> filterGreenApples(List<Apple> appleList) {
        List<Apple> result = new ArrayList<Apple>();
        for (Apple apple: appleList){
            if ("green".equals(apple.getColor())) {
                result.add(apple);
            }
        }
        return result;
    }
}
