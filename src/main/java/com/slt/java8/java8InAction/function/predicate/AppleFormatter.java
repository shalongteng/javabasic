package com.slt.java8.java8InAction.function.predicate;


import com.slt.java8.java8InAction.common.Apple;

public interface AppleFormatter {
    String accept(Apple a);
}