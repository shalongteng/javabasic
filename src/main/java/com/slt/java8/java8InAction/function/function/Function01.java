package com.slt.java8.java8InAction.function.function;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * 它接受一个泛型T的对象，并返回一个泛型R的对象
 */
public class Function01 {
    public static void main(String[] args) {
        // [7, 2, 6]
        List<Integer> list = map(Arrays.asList("lambdas","in","action"),(String s) -> s.length());
        System.out.println(list);
    }

    /**
     * 创建一个map方法，将一个String列表映射到包含每个String长度的Integer列表
     *
     */
    public static <T, R> List<R> map(List<T> list, Function<T, R> f) {
            List<R> result = new ArrayList<>();
            for (T s : list) {
                result.add(f.apply(s));
            }
            return result;
    }
}
