package com.slt.java8.java8InAction.function.example.appleComparator;

import com.slt.java8.java8InAction.common.Apple;

import java.util.Arrays;
import java.util.List;

/**
 * 第 3 步：使用 Lambda 表达式
 */
public class AppleComparator03 {
    public static void main(String[] args) {
        List<Apple> appleList = Arrays.asList(new Apple(80,"green"),
                new Apple(155, "green"),
                new Apple(120, "red"));

//        appleList.sort((Apple apple1,Apple apple2)->apple1.getWeight().compareTo(apple2.getWeight()));
        appleList.sort((apple1,apple2)->apple1.getWeight().compareTo(apple2.getWeight()));
        System.out.println(appleList);
    }


}
