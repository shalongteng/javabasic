package com.slt.java8.java8InAction.function.biFunction;

import java.util.Comparator;
import java.util.function.BinaryOperator;

/**
 * BiFunction的一个特殊例子，接收两个参数，产生一个结果，
 * 只是它的三个参数都是同一个数据类型，这个函数式接口就是BinaryOperator
 */
public class BinaryOperatorTest {
    public static void main(String[] args){
        BinaryOperatorTest test = new BinaryOperatorTest();

        BinaryOperator<Integer> binaryOperator = (Integer num1, Integer num2) -> num1 + num2;
        Integer add = binaryOperator.apply(1, 1);
        System.out.println(add);

        String min = BinaryOperator.minBy((String str1, String str2) -> str1.compareTo(str2)).apply("aaa", "bbb");
        System.out.println(min);
        String max = BinaryOperator.maxBy((String str1, String str2) -> str1.compareTo(str2)).apply("aaa", "bbb");
        System.out.println(max);
    }


    public void testBinaryOperator(Integer num1,Integer num2,BinaryOperator<Integer> result){
        System.out.println(result.apply(num1,num2));
    }

    /**
     * 返回两者里面较小的一个
     */
    public void testMinBy(String str1, String str2, Comparator<String> comparator){
        System.out.println(BinaryOperator.minBy(comparator).apply(str1,str2));
    }

    /**
     * 返回两者里面较大的一个
     */
    public void testMaxBy(String str1, String str2, Comparator<String> comparator){
        System.out.println(BinaryOperator.maxBy(comparator).apply(str1,str2));
    }
}

