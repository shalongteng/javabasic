package com.slt.java8.java8InAction.function.example.appleComparator;

import com.slt.java8.java8InAction.common.Apple;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 第 1 步：传递代码
 */
public class AppleComparator01 {
    public static void main(String[] args) {
        List<Apple> appleList = Arrays.asList(new Apple(80,"green"),
                new Apple(155, "green"),
                new Apple(120, "red"));

        appleList.sort(new AppleComparator());
        System.out.println(appleList);
    }

    static class AppleComparator implements Comparator<Apple> {
        public int compare(Apple a1, Apple a2){
            return a1.getWeight().compareTo(a2.getWeight());
        }
    }
}
