package com.slt.java8.java8InAction.function.methodReference;

import com.slt.java8.java8InAction.common.Apple;

import java.util.Arrays;
import java.util.List;

/**
 * lambda 表达式写法
 */
public class MethodReference01 {
    public static void main(String[] args) {
        List<Apple> appleList = Arrays.asList(new Apple(80,"green"),
                new Apple(155, "green"),
                new Apple(120, "red"));
        appleList.sort((Apple a1, Apple a2)-> a1.getWeight().compareTo(a2.getWeight()));
        System.out.println(appleList);
    }
}
