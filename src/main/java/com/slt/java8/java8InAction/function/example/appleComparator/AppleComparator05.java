package com.slt.java8.java8InAction.function.example.appleComparator;

import com.slt.java8.java8InAction.common.Apple;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 第 5 步：使用方法引用
 */
public class AppleComparator05 {
    public static void main(String[] args) {
        List<Apple> appleList = Arrays.asList(new Apple(80,"green"),
                new Apple(155, "green"),
                new Apple(120, "red"));

        appleList.sort(Comparator.comparing(Apple::getWeight).reversed().thenComparing(Apple::getColor));
        System.out.println(appleList);
    }


}
