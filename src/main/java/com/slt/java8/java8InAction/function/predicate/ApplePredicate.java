package com.slt.java8.java8InAction.function.predicate;


import com.slt.java8.java8InAction.common.Apple;

/**
 * 谓词 即一个返回boolean值的函数
 * 选择苹果的不同策略
 */
@FunctionalInterface
public interface ApplePredicate {
    boolean test(Apple apple);
}