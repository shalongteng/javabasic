package com.slt.java8.java8InAction.function.function;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * compose, andThen
 */
public class Function02 {
    public static void main(String[] args) {
        //7 2 6
        List<String> stringList = Arrays.asList("lambdas", "in", "action");
        Function<String, Integer> lengthFunction = (String s) -> s.length();
        System.out.println(Function.identity());
        Function<Integer, Integer> doubleLengthFunction = (Integer i) -> i * 2;
        //先做传入的Function类型的参数的apply操作，再做当前这个接口的apply操作
        List<Integer> list = map(stringList,doubleLengthFunction.compose(lengthFunction));
        List<Integer> list2 = map(stringList,lengthFunction.andThen(doubleLengthFunction));
        System.out.println(list);
        System.out.println(list2);
    }

    /**
     * 创建一个map方法，将一个String列表映射到包含每个String长度的Integer列表
     */
    public static <T, R> List<R> map(List<T> list, Function<T, R> f) {
            List<R> result = new ArrayList<>();
            for (T s : list) {
                result.add(f.apply(s));
            }
            return result;
    }
}
