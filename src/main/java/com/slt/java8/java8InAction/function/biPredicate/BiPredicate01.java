package com.slt.java8.java8InAction.function.biPredicate;

import java.util.function.BiPredicate;

public class BiPredicate01 {
    public static void main(String[] args) {
        BiPredicate<String, Integer> filter = (x, y) -> {
            return x.length() == y;
        };

        boolean ret = filter.test("cattle", 6);
        boolean ret2 = filter.test("java", 8);
        System.out.println(ret);
        System.out.println(ret2);
    }
}
