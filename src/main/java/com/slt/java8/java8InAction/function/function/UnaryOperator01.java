package com.slt.java8.java8InAction.function.function;

import java.util.function.Function;
import java.util.function.UnaryOperator;

public class UnaryOperator01 {
    public static void main(String[] args) {
        UnaryOperator<Integer> dda = x -> x + 1;
        Function<Integer,Integer> dda1 = x -> x + 1;
        System.out.println(dda.apply(10));// 11
        System.out.println(dda1.apply(10));// 11


        UnaryOperator<String> ddb = x -> x + 1;
        System.out.println(ddb.apply("aa"));// aa1
    }
}
