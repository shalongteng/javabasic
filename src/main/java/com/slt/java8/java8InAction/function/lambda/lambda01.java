package com.slt.java8.java8InAction.function.lambda;

import com.slt.java8.java8InAction.common.Apple;

import java.util.function.Function;

/**
 * 有大括号 ----- 跟普通java写法一样
 *  显示写 return
 *  需要写分号
 *
 *
 * 没有大括号时候
 *  不用写分号
 *  不用写return
 */
public class lambda01 {
    public static void main(String[] args) {
        //lambda 用在接受函数式接口作为参数的 方法调用上。
        System.out.println(add(() -> 1));
        System.out.println(add(() -> {return 1;}));
        //lambda 返回一个函数
        Function<Apple, Integer> appleIntegerFunction = (Apple a) -> a.getWeight();
        int a =0,b=0,c =0 ;
        c = a>b? a:b;

    }

    public static Integer add(IntLambda intLambda){
        return intLambda.test() + 1;
    }

    @FunctionalInterface
    static interface IntLambda{
        Integer test();
    }
}
