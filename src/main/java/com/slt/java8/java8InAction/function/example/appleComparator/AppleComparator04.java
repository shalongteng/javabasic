package com.slt.java8.java8InAction.function.example.appleComparator;

import com.slt.java8.java8InAction.common.Apple;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 第 4 步：使用 Lambda 表达式
 */
public class AppleComparator04 {
    public static void main(String[] args) {
        List<Apple> appleList = Arrays.asList(new Apple(80,"green"),
                new Apple(155, "green"),
                new Apple(120, "red"));

        appleList.sort(Comparator.comparing((Apple a) -> a.getWeight()));
        System.out.println(appleList);
    }


}
