package com.slt.java8.java8InAction.function.biFunction;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

/**
 * BiFunction 接受两个参数 返回一个值
 */
public class BiFunction01 {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("name", "shalongteng");
        BiFunction<String, String, String> biFunction = (k, v) -> k + " : " + map.get(k).toUpperCase();
        System.out.println(map.compute("name", biFunction));

    }
}
