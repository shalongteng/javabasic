package com.slt.java8.java8InAction.function.example;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 */
public class FileUtil {
    public static void main(String[] args) throws IOException {
        String oneLine = processFile02((BufferedReader br) -> br.readLine());
        String twoLines = processFile02((BufferedReader br) -> br.readLine() + br.readLine());
    }

    public static String processFile01() throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader("data.txt"))) {
            //这里是行为 将此处抽取出来 (BufferedReader br) -> br.readLine() + br.readLine()
            //想要抽取出此处行为，就只能将br作为参数，传递给一个方法，由此构建一个函数式接口
            return br.readLine();
        }
    }
    //行为参数化改造
    //你需要把行为传递给processFile02，以便它可以利用BufferedReader执行不同的行为。
    public static String processFile02(BufferedReaderProcessor p) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader("data.txt"))) {
            return p.process(br);
        }
    }


    @FunctionalInterface
    interface BufferedReaderProcessor {
        String process(BufferedReader b) throws IOException;
    }
}
