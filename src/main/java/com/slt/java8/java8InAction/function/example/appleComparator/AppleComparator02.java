package com.slt.java8.java8InAction.function.example.appleComparator;

import com.slt.java8.java8InAction.common.Apple;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 第 2 步：使用匿名类
 */
public class AppleComparator02 {
    public static void main(String[] args) {
        List<Apple> appleList = Arrays.asList(new Apple(80,"green"),
                new Apple(155, "green"),
                new Apple(120, "red"));

        appleList.sort(new Comparator<Apple>() {
            @Override
            public int compare(Apple o1, Apple o2) {
                return o1.getWeight().compareTo(o2.getWeight());
            }
        });
        System.out.println(appleList);
    }


}
