package com.slt.java8.java8InAction.function.methodReference;

import java.util.Arrays;

/**
 * 引用某个类实例方法
 */
public class Reference02 {
    public static void main(String[] args) {
        String[] array = new String[] { "Apple", "Orange", "Banana", "Lemon" };
        //实例方法有一个隐含的this参数
        Arrays.sort(array, String::compareTo);
        System.out.println(String.join(", ", array));


    }

}
