package com.slt.java8.java8InAction.function.methodReference;

import com.slt.java8.java8InAction.common.Apple;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiPredicate;

/**
 * 方法签名决定怎么使用方法引用
 * 可以从方法引用往回倒到lambda 来理解
 *
 * 静态方法
 *    省略了方法参数
 * 普通方法
 *    省略了第一个调用者参数，以及n个方法参数
 */
public class MethodReference02 {
    public static void main(String[] args) {
        List<Apple> appleList = Arrays.asList(new Apple(80,"green"),
                new Apple(155, "green"),
                new Apple(120, "red"));
        //lambda  方法签名是 接受一个参数T 返回一个R
        appleList.sort(Comparator.comparing((Apple apple) -> apple.getWeight()));
        //普通方法 (Apple apple)->apple.getWeight() 省略调用者一个参数 省略了n个方法参数
        appleList.sort(Comparator.comparing(Apple::getWeight));
        //静态方法  (Apple apple) -> Apple.getWeight2(apple)  省略了方法参数
        appleList.sort(Comparator.comparing(Apple::getWeight2));
        System.out.println(appleList);

        //普通方法的 方法引用
        BiPredicate<List<String>, String> contains =
                (list, element) -> list.contains(element);
        //这个Lambda使用其第一个参数，调用其contains方法。由于第一个参数是List类型
        BiPredicate<List<String>, String> contains2 = List::contains;
        //(list,element)-> list.contains(element) 省略了第一个调用者参数，第二个方法参数
    }
}
