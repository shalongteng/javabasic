package com.slt.java8.java8InAction.function.predicate;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class PredicateTest {
    public static void main(String[] args) {
        Predicate<String> nonEmptyStringPredicate = (String s) -> !s.isEmpty();
        Function<String,Boolean> nonEmptyStringFunction = (String s) -> !s.isEmpty();
        List<String> listOfStrings = null;
        List<String> nonEmpty = filter(listOfStrings, nonEmptyStringPredicate);
    }
    public static <T> List<T> filter(List<T> list, Predicate<T> p) {
        List<T> results = new ArrayList<>();
        for(T s: list){
            if(p.test(s)){
                results.add(s);
            }
        }
        return results;
    }
  
}
