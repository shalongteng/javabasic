package com.slt.java8.java8InAction.function.methodReference;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 引用某个类静态方法。
 * 所谓方法引用，是指如果某个方法签名和接口恰好一致，就可以直接传入方法引用。
 */
public class Reference01 {
    public static void main(String[] args) {
        String[] array = new String[] { "Apple", "Orange", "Banana", "Lemon" };
        //指向静态方法的方法引用。
        Arrays.sort(array, Reference01::cmp);
        //连接指定数组的元素或集合的成员，在每个元素或成员之间使用指定的分隔符。
        System.out.println(String.join(", ", array));


        /**
         * 匿名函数写法
         */
        Arrays.sort(array, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        }.reversed());
        System.out.println(String.join(", ", array));
    }

    static int cmp(String s1, String s2) {
        return s1.compareTo(s2);
    }
}
