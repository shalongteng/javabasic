package com.slt.java8.java8InAction.function.function;

public class Task {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
