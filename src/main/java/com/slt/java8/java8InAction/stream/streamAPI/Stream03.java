package com.slt.java8.java8InAction.stream.streamAPI;

import com.slt.java8.java8InAction.stream.Menu;
import com.slt.java8.java8InAction.stream.Dish;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * map flatMap
 */
public class Stream03 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();
        //找出每道菜的名称有多长
        List<Integer> dishNameLengths = menu.stream()
                .map(Dish::getName)
                .map(String::length)
                .collect(toList());
        System.out.println(dishNameLengths);
    }


}
