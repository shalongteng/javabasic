package com.slt.java8.java8InAction.stream.streamAPI.reduce;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;

import java.util.List;
import java.util.Optional;

/**
 * 怎样用map和reduce方法数一数流中有多少个菜呢?
 *
 * map和reduce的连接通常称为map-reduce模式
 */
public class Reduce04 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();
        long count = menu.stream().count();
        System.out.println(count);


        Optional<Integer> reduce = menu.stream().map(dish -> 1).reduce(Integer::sum);
        System.out.println(reduce.get());

    }
}
