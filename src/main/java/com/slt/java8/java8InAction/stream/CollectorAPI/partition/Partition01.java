package com.slt.java8.java8InAction.stream.CollectorAPI.partition;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;
import com.slt.java8.java8InAction.stream.streamAPI.Type;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.partitioningBy;

/**
 * 由一个谓词（返回一个布尔值的函数）作为分类函数
 */
public class Partition01 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();
        Map<Boolean, List<Dish>> partitionedMenu =
                menu.stream().collect(partitioningBy(Dish::isVegetarian));
        System.out.println(partitionedMenu);

        Map<Boolean, Map<Type, List<Dish>>> vegetarianDishesByType =
                menu.stream().collect(
                        partitioningBy(Dish::isVegetarian,
                                groupingBy(Dish::getType)));
    }
}
