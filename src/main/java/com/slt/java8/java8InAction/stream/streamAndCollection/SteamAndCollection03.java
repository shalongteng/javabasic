package com.slt.java8.java8InAction.stream.streamAndCollection;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * 中间操作,终端操作
 * 很多菜的热量都高于300卡路里，但只选出了前三个！limit操作和一种称为短路的技巧，
 * 尽管filter和map是两个独立的操作，但它们合并到同一次遍历中了（循环合并）。
 */
public class SteamAndCollection03 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();

        List<String> names =
                menu.stream()
                        .filter(d -> {
                            System.out.println("filtering" + d.getName());
                            return d.getCalories() > 300;
                        })
                        .map(d -> {
                            System.out.println("mapping" + d.getName());
                            return d.getName();
                        })
                        .limit(3)
                        .collect(toList());
        System.out.println(names);
    }


}
