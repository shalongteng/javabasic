package com.slt.java8.java8InAction.stream.streamAPI.createStream;

import java.util.Arrays;

/**
 * 由数组创建流
 */
public class CreateStream02 {
    public static void main(String[] args) {
        int[] numbers = {2, 3, 5, 7, 11, 13};
        int sum = Arrays.stream(numbers).sum();
    }
}
