package com.slt.java8.java8InAction.stream.streamAPI.collect;

import com.slt.java8.java8InAction.stream.exercise.Exercise02;
import com.slt.java8.java8InAction.stream.exercise.Transaction;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 用指令式风格  对交易按照货币分组
 */
public class Collect01 {
    public static void main(String[] args) {
        Map<Currency, List<Transaction>> transactionsByCurrencies =
                new HashMap<>();
        //获取所有的交易
        List<Transaction> transactions = Exercise02.init();

        for (Transaction transaction : transactions) {
            Currency currency = transaction.getCurrency();
            List<Transaction> transactionsForCurrency =
                    transactionsByCurrencies.get(currency);
            if (transactionsForCurrency == null) {
                transactionsForCurrency = new ArrayList<>();
                transactionsByCurrencies
                        .put(currency, transactionsForCurrency);
            }
            transactionsForCurrency.add(transaction);
        }


        /**
         * 用声明式风格  对交易按照货币分组
         * 函数式编程
         * 指令式编程
         */
        Map<Currency, List<Transaction>> collect =
                transactions.stream()
                        .collect(Collectors.groupingBy(Transaction::getCurrency));
    }
}
