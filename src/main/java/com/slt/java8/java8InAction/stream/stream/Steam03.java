package com.slt.java8.java8InAction.stream.stream;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;

import java.util.List;
import java.util.stream.Collectors;

/**
 *
 */
public class Steam03 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();
        List<String> list = menu.stream().filter(dish -> dish.getCalories() > 300)
                .map(dish -> dish.getName())
                .limit(3)
                .collect(Collectors.toList());

        System.out.println(list);
    }




}
