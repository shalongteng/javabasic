package com.slt.java8.java8InAction.stream.streamAPI.reduce;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * 求最大值
 */
public class Reduce03 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        //使用初始值 和后边每一个数都去做同样操作 得到最终的值 返回
        int sum = numbers.stream().reduce(10, Integer::max);
        Optional<Integer> optional = numbers.stream().reduce(Integer::max);

        System.out.println(sum);
        System.out.println(optional.get());
    }
}
