package com.slt.java8.java8InAction.stream.streamAPI.flatmap;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * 对于一张单词表，如何返回一张列表，列出里面各不相同的字符
 * ["Hello","World"]，返回列表["H","e","l", "o","W","r","d"]
 * <p>
 * 错误版本 无法解决
 */
public class FlatMap02 {
    public static void main(String[] args) {
        //你需要一个字符流，而不是数组流
        String[] arrayOfWords = {"Goodbye", "World"};
        Arrays.stream(arrayOfWords);

        List<String> words = Arrays.asList("Hello", "World");

        List<Stream<String>> collect = words.stream()
                .map(word -> word.split(""))
                .map(Arrays::stream)//将数组 转换成流
                .distinct()
                .collect(toList());
        //生成了三个流
        for (Stream<String> stringStream : collect) {
            stringStream.forEach(s -> System.out.print(s));
        }
    }
}
