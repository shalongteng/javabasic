package com.slt.java8.java8InAction.stream.myCollector;

import java.util.*;

public class PrimeNumbersCollectorTest {
    public static void main(String[] args) {
        /**
         * 就是个继承hashmap的 匿名内部类
         */
        HashMap<Boolean, List<Integer>> hashMap = new HashMap<Boolean, List<Integer>>() {{
            put(true, new ArrayList<Integer>());
            put(false, new ArrayList<Integer>());
        }};
        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();

        System.out.println(hashMap.getClass());
        System.out.println(new Runnable() {
            @Override
            public void run() {

            }
        }.getClass());
        System.out.println(new Hello().getClass());
        System.out.println(objectObjectHashMap.getClass());



        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < 10; i++) {
            long start = System.nanoTime();
            partitionPrimes(1_000_000);
            long duration = (System.nanoTime() - start) / 1_000_000;
            if (duration < fastest) fastest = duration;
        }
        System.out.println(
                "Fastest execution done in " + fastest + " msecs");
    }

    private static void partitionPrimes(int i) {

    }
    static class Hello{
        public Hello() {
        }
    }
}