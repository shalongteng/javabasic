package com.slt.java8.java8InAction.stream.CollectorAPI.group;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;
import com.slt.java8.java8InAction.stream.streamAPI.Type;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

/**
 * 多级分组
 * Collectors.mapping
 */
public class Group03 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();

        /**
         *  mapping
         * 累加之前对每个输入元素应用一个映射函数，这样就可以让接受特定类型元素的收集器适应不同类型
         * 的对象
         */
        Map<Type, Set<CaloricLevel>> caloricLevelsByType =
                menu.stream().collect(
                        groupingBy(Dish::getType, Collectors.mapping(
                                dish -> {
                                    if (dish.getCalories() <= 400) return CaloricLevel.DIET;
                                    else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
                                    else return CaloricLevel.FAT;
                                },
                                toSet())));
        System.out.println(caloricLevelsByType);

        //这里set 实现类定死了是 hashset
        Map<Type, Set<CaloricLevel>> caloricLevelsByType2 =
                menu.stream().collect(
                        groupingBy(Dish::getType, mapping(
                                dish -> {
                                    if (dish.getCalories() <= 400) return CaloricLevel.DIET;
                                    else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
                                    else return CaloricLevel.FAT;
                                },
                                toCollection(HashSet::new))));
    }

    public enum CaloricLevel {DIET, NORMAL, FAT}
}
