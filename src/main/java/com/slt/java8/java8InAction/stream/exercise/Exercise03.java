package com.slt.java8.java8InAction.stream.exercise;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 创建一个勾股数流
 *
 * map 里最好别返回 stream流，如果返回stream流
 * 可以尝试使用flatmap 合并流。
 */
public class Exercise03 {
    public static void main(String[] args) {
        getThree();

        Stream<int[]> stream = IntStream.rangeClosed(1, 100)
                .boxed()
                //flatmap 里有多个stream流 合并在一起
                .flatMap(a -> IntStream.rangeClosed(a, 100)
                        .boxed()
                        .filter(b -> Math.sqrt(a * a + b * b) % 1 == 0)
                        .map(b -> new int[]{a, b, (int) Math.sqrt(a * a + b * b)})

                );

        stream.limit(5)
                .forEach(a-> System.out.println(Arrays.toString(a)));

    }

    public static void getThree(){
        Stream<int[]> pythagoreanTriples =
                IntStream.rangeClosed(1, 100).boxed()
                        .flatMap(a ->
                                IntStream.rangeClosed(a, 100)
                                        .filter(b -> Math.sqrt(a*a + b*b) % 1 == 0)
                                        .mapToObj(b -> new int[]{a, b, (int)Math.sqrt(a * a + b * b)})
                        );

        pythagoreanTriples
                .limit(5)
                .forEach(a-> System.out.println(Arrays.toString(a)));
    }
}
