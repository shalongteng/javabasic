package com.slt.java8.java8InAction.stream.streamAPI.builder;

import java.util.stream.Stream;

public class Build01 {
    public static void main(String[] args) {
        Stream.Builder<String> builder = Stream.builder();
        builder.add("Production");
        builder.add("Marketing");
        builder.add("Finance");
        builder.add("Sales");
        builder.add("Operations");
        Stream<String> stream = builder.build();
        stream.forEach(System.out::println);
    }
}
