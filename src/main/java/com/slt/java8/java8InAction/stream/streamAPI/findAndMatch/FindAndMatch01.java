package com.slt.java8.java8InAction.stream.streamAPI.findAndMatch;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;

import java.util.List;

public class FindAndMatch01 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();
        //anyMatch 只要存在一个
        if(menu.stream().anyMatch(Dish::isVegetarian)){
            System.out.println("The menu is (somewhat) vegetarian friendly!!");
        }

        //所有的都要符合条件
        if(menu.stream().allMatch(dish -> dish.getCalories() < 1000)){
            System.out.println("所有菜的热量都低于1000卡路里");
        }
        //一个也不能匹配
        if(menu.stream().noneMatch(dish -> dish.getCalories() > 1000)){
            System.out.println("所有菜的热量都低于1000卡路里");
        }
    }
}
