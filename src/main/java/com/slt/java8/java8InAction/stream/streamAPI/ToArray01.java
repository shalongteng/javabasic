package com.slt.java8.java8InAction.stream.streamAPI;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;

import java.util.Arrays;
import java.util.List;

public class ToArray01 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();
        Object[] objects = menu.stream().toArray();

        Dish[] dishes = menu.stream().filter(p -> p.getCalories() > 500).toArray(Dish[]::new);
        System.out.println(Arrays.toString(dishes));

        /**
         * (length) -> new String[length];
         */
        List<String> list = Arrays.asList("a", "b", "c");
        list.stream().filter(item->item.equals("a")).toArray(String[]::new);

    }
}
