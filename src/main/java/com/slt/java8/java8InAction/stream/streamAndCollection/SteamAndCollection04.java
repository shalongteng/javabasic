package com.slt.java8.java8InAction.stream.streamAndCollection;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;

import java.util.List;

/**
 * 终端操作
 */
public class SteamAndCollection04 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();
        //实例方法省略了方法参数
        menu.stream().forEach(System.out::println);
        menu.stream().forEach(dish -> System.out.println(dish));
    }


}
