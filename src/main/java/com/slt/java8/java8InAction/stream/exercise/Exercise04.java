package com.slt.java8.java8InAction.stream.exercise;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * 斐波纳契元组序列
 *
 */
public class Exercise04 {
    public static void main(String[] args) {
        //依次对每个新生成的值应用函数的
        Stream.iterate(new int[]{0, 1},t -> new int[]{t[1], t[0]+t[1]})
                .limit(10)
                .forEach(a-> System.out.println(Arrays.toString(a)));

    }


}
