package com.slt.java8.java8InAction.stream.stream;

import com.slt.java8.java8InAction.stream.Dish;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 */
public class Steam02 {
    public static void main(String[] args) {

    }

    /**
     * java8 写法
     * 代码是以声明性方式写的：说明想要完成什么（筛选热量低的菜肴）而不是说明如何实现
     */
    public static List<String> getLowCaloric(List<Dish> menu){
        List<String> lowCaloricDishesName =
                menu.stream()
                        .filter(d -> d.getCalories() < 400)
                        .sorted(Comparator.comparing(Dish::getCalories))
                        .map(Dish::getName)
                        .collect(Collectors.toList());

        return lowCaloricDishesName;
    }

    /**
     * 利用多核架构并行执行这段代码，你只需要把stream()换成parallelStream()：
     */
    public static List<String> getLowCaloric2(List<Dish> menu){
        List<String> lowCaloricDishesName =
                menu.parallelStream()
                        .filter(d -> d.getCalories() < 400)
                        .sorted(Comparator.comparing(Dish::getCalories))
                        .map(Dish::getName)
                        .collect(Collectors.toList());

        return lowCaloricDishesName;
    }


}
