package com.slt.java8.java8InAction.stream.streamAndCollection;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * 集合是一种内存中的数据结构，包含数据结构中目前所有的值
 * Stream API仅用于处理数据组。它不会修改实际的集合，它们仅根据流水线方法提供结果。
 *
 * 流只能遍历一次
 * 流只能消费一次！
 */
public class SteamAndCollection01 {
    public static void main(String[] args) {
        List<String> title = Arrays.asList("Java8", "In", "Action");
        Stream<String> s = title.stream();
        s.forEach(System.out::println);
        s.forEach(System.out::println);
    }




}
