package com.slt.java8.java8InAction.stream.CollectorAPI.group;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;
import com.slt.java8.java8InAction.stream.streamAPI.Type;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.*;

/**
 * 多级分组
 * 两个参数 groupingby 可以自定义key value
 */
public class Group02 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();
        Map<Type, Map<CaloricLevel, List<Dish>>> dishesByTypeCaloricLevel =
                menu.stream().collect(
                        groupingBy(Dish::getType,
                                groupingBy(dish -> {
                                    if (dish.getCalories() <= 400) return CaloricLevel.DIET;
                                    else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
                                    else return CaloricLevel.FAT;
                                } )
                        )
                );
        System.out.println(dishesByTypeCaloricLevel);
        //两个参数 groupingby 可以自定义key value、
        Map<Type, Long> typesCount = menu.stream().collect(
                groupingBy(Dish::getType, counting()));
        System.out.println(typesCount);


        Map<Type, Dish> mostCaloricByType =
                menu.stream()
                        .collect(groupingBy(Dish::getType,
                                collectingAndThen(
                                        maxBy(comparingInt(Dish::getCalories)),
                                        Optional::get)));
    }
    public enum CaloricLevel { DIET, NORMAL, FAT }
}
