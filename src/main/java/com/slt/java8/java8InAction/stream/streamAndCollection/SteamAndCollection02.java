package com.slt.java8.java8InAction.stream.streamAndCollection;

import com.slt.java8.java8InAction.stream.Menu;
import com.slt.java8.java8InAction.stream.Dish;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 外部迭代与内部迭代
 * Collection接口需要用户去做迭代（比如用for-each），这称为外部迭代。
 * Streams库使用内部迭代——它帮你把迭代做了
 * 内部迭代优点：
 *   可以透明地并行处理
 *   用更优化的顺序进行处理
 */
public class SteamAndCollection02 {
    public static void main(String[] args) {
        //for-each 外部迭代
        List<Dish> menu = Menu.getMenu();
        List<String> names = new ArrayList<>();
        for(Dish d: menu){
            names.add(d.getName());
        }
        System.out.println(names);

        //显示迭代
        List<String> names2 = new ArrayList<>();
        Iterator<Dish> iterator = menu.iterator();
        while(iterator.hasNext()) {
            Dish d = iterator.next();
            names2.add(d.getName());
        }
        System.out.println(names2);

        //内部迭代
        List<String> list = menu.stream().map(dish -> dish.getName()).collect(Collectors.toList());
        System.out.println(list);
    }




}
