package com.slt.java8.java8InAction.stream.CollectorAPI.group;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;
import com.slt.java8.java8InAction.stream.streamAPI.Type;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Group01 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();
        //按照食物类型分组
        Map<Type, List<Dish>> collect = menu.stream().collect(Collectors.groupingBy(Dish::getType));
        System.out.println(collect);


        //
        Map<String, List<Dish>> dishesByCaloricLevel = menu.stream().collect(
                Collectors.groupingBy(dish -> {
                    if (dish.getCalories() <= 400) return "diet";
                    else if (dish.getCalories() <= 700) return
                            "normal";
                    else return "fat";
                } ));
        System.out.println(dishesByCaloricLevel);
    }

}
