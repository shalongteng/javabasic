package com.slt.java8.java8InAction.stream.streamAPI.mapTo;

import java.util.stream.IntStream;

/**
 *
 */
public class MapToInt02 {
    public static void main(String[] args) {
        long count = IntStream.rangeClosed(1, 100)
                .filter(n -> n % 2 == 0)
                .count();

        System.out.println(count);


    }
}
