package com.slt.java8.java8InAction.stream.exercise;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * (1) 找出2011年发生的所有交易，并按交易额排序(从低到高)。
 * (2) 交易员都在哪些不同的城市工作过?
 * (3) 查找所有来自于剑桥的交易员，并按姓名排序。
 * (4) 返回所有交易员的姓名字符串，按字母顺序排序。
 * (5) 有没有交易员是在米兰工作的?
 * (6) 打印生活在剑桥的交易员的所有交易额。
 * (7) 所有交易中，最高的交易额是多少?
 * (8) 找到交易额最小的交易。
 *
 * 总结 跟写sql类似
 *  但是需要先写  条件filter 后写查询哪些列map
 */
public class Exercise02 {
    public static void main(String[] args) {
        List<Transaction> transactionList = init();

        get2011(transactionList);
        getCity(transactionList);
        getTrader(transactionList);
        getTraderName(transactionList);
        isMilan(transactionList);
        getSumValue(transactionList);
        System.out.println();
        getMaxValue(transactionList);
        getMinValue(transactionList);
    }

    /**
     * (1) 找出2011年发生的所有交易，并按交易额排序(从低到高)。
     * Transaction::getValue
     *  使用普通方法 方法引用 省略调用者参数
     *
     */
    public static void get2011(List<Transaction> transactionList) {
        List<Transaction> transactions = transactionList.stream()
                .filter(t -> t.getYear() == 2011)
//                .sorted(Comparator.comparingInt(t->t.getValue()))
                .sorted(Comparator.comparingInt(Transaction::getValue))
                .collect(toList());
        System.out.println(transactions);
    }

    /**
     * (2) 交易员都在哪些不同的城市工作过?
     * 这里也可以是.collect(toSet());   也可以达到去重效果
     */
    public static void getCity(List<Transaction> transactionList) {
        List<String> collect = transactionList.stream()
                .map(t -> t.getTrader().getCity())
                .distinct()
                .collect(toList());
        System.out.println(collect);
    }

    /**
     * (3) 查找所有来自于剑桥的交易员，并按姓名排序。
     */
    public static void getTrader(List<Transaction> transactionList) {
        List<Trader> traderList = transactionList.stream()
                .map(Transaction::getTrader)
                .filter(t -> t.getCity().equals("Cambridge"))
                .sorted(Comparator.comparing(Trader::getName))
                .distinct()
                .collect(toList());
        System.out.println(traderList);
    }

    /**
     * (4) 返回所有交易员的姓名字符串，按字母顺序排序。
     */
    public static void getTraderName(List<Transaction> transactionList) {
        String collect = transactionList.stream()
                .map(t -> t.getTrader().getName())
                .distinct()
                .sorted()
                .reduce("",(n1, n2) -> n1 + n2 + " ");
        System.out.println(collect);
    }

    /**
     * (5) 有没有交易员是在米兰工作的?
     */
    public static void isMilan(List<Transaction> transactionList) {
        boolean milan = transactionList.stream()
                .anyMatch(t -> t.getTrader().getCity().equals("Milan"));
        System.out.println("milan: "+milan);
    }


    /**
     * (6) 打印生活在剑桥的交易员的所有交易额。
     */
    public static void getSumValue(List<Transaction> transactionList) {
        transactionList.stream()
                .filter(t ->"Cambridge".equals(t.getTrader().getCity()))
                .map(Transaction::getValue)
                .forEach(v-> System.out.print(v+" "));

    }
    /**
     * (7) 所有交易中，最高的交易额是多少?
     */
    public static void getMaxValue(List<Transaction> transactionList) {
        transactionList.stream()
                .map(Transaction::getValue)
                .reduce(Integer::max)
                .ifPresent(v-> System.out.println("最高的交易额是: "+v));

    }

    /**
     * (8) 找到交易额最小的交易。
     */
    public static void getMinValue(List<Transaction> transactionList) {
        transactionList.stream()
                .reduce((t1,t2)-> t1.getValue()  > t2.getValue()?t1:t2)
                .map(t-> "交易额最小的交易是: "+ t)
                .ifPresent(System.out::println);

    }

    //初始化数据
    public static List<Transaction> init() {
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario", "Milan");
        Trader alan = new Trader("Alan", "Cambridge");
        Trader brian = new Trader("Brian", "Cambridge");

        List<Transaction> transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );
        return transactions;
    }


}
