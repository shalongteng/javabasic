package com.slt.java8.java8InAction.stream.streamAPI;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;

import java.util.Arrays;
import java.util.List;
import static java.util.stream.Collectors.toList;

/**
 * 筛选和切片
 * filter
 */
public class Stream01 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();

        getDistinctInt();
    }

    /**
     * filter
     */
    private static void getvegetarianMenu(List<Dish> menu) {
        List<Dish> vegetarianMenu = menu.stream()
                .filter(Dish::isVegetarian)
                .collect(toList());
    }

    /**
     * distinct 去重
     */
    private static void getDistinctInt() {
        List<Integer> numbers = Arrays.asList(1, 2, 1, 4, 3, 2, 4);
        numbers.stream()
                .filter(i -> i % 2 == 0)
                .distinct()
                .forEach(System.out::println);
    }
}
