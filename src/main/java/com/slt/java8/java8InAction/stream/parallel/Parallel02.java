package com.slt.java8.java8InAction.stream.parallel;

import java.util.stream.LongStream;

public class Parallel02 {
    public static void main(String[] args) {

    }
    public static long sideEffectSum(long n) {
        Accumulator accumulator = new Accumulator();
        LongStream.rangeClosed(1, n).forEach(accumulator::add);
        return accumulator.total;
    }
    static class Accumulator {
        public long total = 0;
        public void add(long value) { total += value; }
    }
}
