package com.slt.java8.java8InAction.stream.streamAPI;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * skip 跳过元素
 * 返回一个扔掉了前n个元素的流。如果流中元素不足n个，则返回一
 * 个空流。请注意，limit(n)和skip(n)是互补的！
 */
public class Stream02 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();

        List<Dish> dishes = menu.stream()
                .filter(d -> d.getCalories() > 300)
                .skip(2)
                .collect(toList());

        List<Dish> dishes2 = menu.stream()
                .filter(d -> d.getCalories() > 300)
                .collect(toList());
        System.out.println(dishes);
        System.out.println(dishes2);
    }


}
