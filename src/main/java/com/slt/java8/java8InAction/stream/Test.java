package com.slt.java8.java8InAction.stream;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {
    public static void main(String[] args) {
        Map map = new HashMap();
        map.put("a","a");
        /**
         * merge
         * 如果有这个key，把oldValue和newValue 做一个函数操作放入map中
         * 如果没有这个key 把key value放进去
         */
        map.merge("a","b",( a, b)->(String)a + b);
        map.merge("b","b",( a, b)->(String)a + b);
        System.out.println(map);
    }
}
