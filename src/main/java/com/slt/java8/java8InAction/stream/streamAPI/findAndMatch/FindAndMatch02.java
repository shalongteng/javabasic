package com.slt.java8.java8InAction.stream.streamAPI.findAndMatch;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;

import java.util.List;
import java.util.Optional;

/**
 * Optional01 是一个容器类
 */
public class FindAndMatch02 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();
        //利用短路找到结果时立即结束
        Optional<Dish> dish =
                menu.stream()
                        .filter(Dish::isVegetarian)
                        .findAny();
        System.out.println(dish.get());

        /**
         * findFirst 在并行上限制更多
         * findAny 它在使用并行流时限制较少。
         */
        menu.stream()
                .filter(Dish::isVegetarian)
                .findFirst()
                .ifPresent(d -> System.out.println(d.getName()));
    }
}
