package com.slt.java8.java8InAction.stream.streamAPI.reduce;

import java.util.Arrays;
import java.util.List;

/**
 * 最终将 多个值规约成一个数
 */
public class Reduce01 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        int sum = 0;
        for (int x : numbers) {
            sum += x;
        }
        System.out.println(sum);

        /**
         * reduce接受两个参数：
         * 一个初始值，这里是0；
         * 一个BinaryOperator<T>来将两个元素结合起来产生一个新值，这里我们用的是
         * lambda (a, b) -> a + b。
         */
        int sum2 = numbers.stream().reduce(0, (a, b) -> a + b);
        System.out.println(sum2);


        int product = numbers.stream().reduce(1, (a, b) -> a * b);
        System.out.println(product);
    }
}
