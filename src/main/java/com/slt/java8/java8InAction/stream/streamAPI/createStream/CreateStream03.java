package com.slt.java8.java8InAction.stream.streamAPI.createStream;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * 由文件生成流
 */
public class CreateStream03 {
    public static void main(String[] args) {
        long uniqueWords = 0;
        /**
         * try括号内的资源会在try语句结束后自动释放，前提是这些可关闭的资源必须实现 java.lang.AutoCloseable 接口。
         * InputStream 和OutputStream 父类中一定实现了AutoCloseable接口
         */
        try (Stream<String> lines =
                     Files.lines(Paths.get("app.xml"), Charset.defaultCharset())) {

            Stream<String> stringStream = lines.flatMap(line -> Arrays.stream(line.split(" ")));
            long count = stringStream.distinct().count();
            System.out.println(count);
        } catch (IOException e) {
        }
    }
}
