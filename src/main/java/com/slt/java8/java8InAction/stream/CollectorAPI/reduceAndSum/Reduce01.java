//package com.slt.java8.java8InAction.stream.CollectorAPI.reduceAndSum;
//
//
//import java.util.IntSummaryStatistics;
//import java.util.List;
//import java.util.Optional;
//import java.util.stream.Collectors;
//
//import com.slt.java8.java8InAction.stream.Dish;
//import com.slt.java8.java8InAction.stream.Menu;
//
//import static java.util.stream.Collectors.joining;
//
///**
// * 归约操作
// */
//public class Reduce01 {
//    public static void main(String[] args) {
//        List<Dish> menu = Menu.getMenu();
////        getMenuCount(menu);
//        getMenuMax(menu);
////        getMenuSum(menu);
////        getMenuSummarize(menu);
////        getMenuJoining(menu);
//    }
//
//    /**
//     * Collectors.counting() = stream().count()
//     */
//    public static void getMenuCount(List<Dish> menu) {
//        //这两个等价
//        Long collect = menu.stream().collect(Collectors.counting());
//        long count = menu.stream().count();
//        System.out.println(collect);
//    }
//
//    public static void getMenuMax(List<Dish> menu) {
//        //Optional01 它是一个容器
////        Optional<Dish> collect = menu.stream().collect(Collectors.maxBy(Comparator.comparing(Dish::getCalories)));
////        long count = menu.stream().count();
////        System.out.println(collect);
//        /**
//         * 单参数形式的reducing
//         */
//        Optional<Dish> mostCalorieDish =
//                menu.stream().collect(Collectors.reducing(
//                        (d1, d2) -> d1.getCalories() > d2.getCalories() ? d1 : d2));
//
//    }
//
//    public static void getMenuSum(List<Dish> menu) {
//        //Optional01 它是一个容器
//        Integer collect = menu.stream().collect(Collectors.summingInt(Dish::getCalories));
//        long count = menu.stream()
//                .mapToInt(Dish::getCalories)
//                .sum();
//        System.out.println(collect);
//        System.out.println(count);
//        /**
//         * 第一个参数是归约操作的起始值
//         * 第二个参数 将菜肴转换成一个表示其所含热量的int。 类似于map
//         * 第三个参数是一个BinaryOperator，将两个int求和。
//         */
//        int totalCalories第一个参数是归约操作的起始值 = menu.stream().collect(Collectors.reducing(
//                0, Dish::getCalories, (i, j) -> i + j));
//    }
//
//    /**
//     * 汇总
//     * @param menu
//     */
//    public static void getMenuSummarize(List<Dish> menu) {
//        //Optional01 它是一个容器
//        IntSummaryStatistics collect = menu.stream().collect(Collectors.summarizingInt(Dish::getCalories));
//        long count = menu.stream()
//                .mapToInt(Dish::getCalories)
//                .sum();
//        System.out.println(collect);
//        System.out.println(count);
//    }
//    public static void getMenuJoining(List<Dish> menu) {
//        //Optional01 它是一个容器
//        String shortMenu = menu.stream().map(Dish::getName).collect(joining(", "));
//        System.out.println(shortMenu);
//    }
//
//    public static void reducing(List<Dish> menu) {
//        Integer collect = menu.stream().collect(Collectors.reducing(0, //初始值
//                Dish::getCalories,//转换函数
//                Integer::sum));//累积函数
//    }
//
//
//}
