package com.slt.java8.java8InAction.stream.streamAPI.mapTo;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;

import java.util.List;
import java.util.OptionalInt;

/**
 *
 */
public class MapToInt01 {
    public static void main(String[] args) {
        List<Dish> menu = Menu.getMenu();
        int sum = menu.stream()
                .mapToInt(Dish::getCalories)
                .sum();//转换成 int流 就可以使用sum方法了

        menu.stream()
                .mapToInt(Dish::getCalories)
                .boxed();

        OptionalInt maxCalories = menu.stream()
                .mapToInt(Dish::getCalories)
                .max();

        maxCalories.orElse(8);
        System.out.println(maxCalories);

        System.out.println(sum);
    }
}
