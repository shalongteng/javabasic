package com.slt.java8.java8InAction.stream.stream;

import com.slt.java8.java8InAction.stream.Dish;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Steam01 {
    public static void main(String[] args) {

    }

    /**
     * java8 之前写法
     * 返回低热量的菜肴名称
     */
    public static List<String> getLowCaloric(List<Dish> menu){
        List<Dish> lowCaloricDishes = new ArrayList<>();
        //过滤热量低于400 的食物
        for(Dish d: menu){
            if(d.getCalories() < 400){
                lowCaloricDishes.add(d);
            }
        }
        //给低热量食物排序
        Collections.sort(lowCaloricDishes, new Comparator<Dish>() {
            public int compare(Dish d1, Dish d2){
                return Integer.compare(d1.getCalories(), d2.getCalories());
            }
        });
        //过滤出低热量食物名称，并返回
        List<String> lowCaloricDishesName = new ArrayList<>();
        for(Dish d: lowCaloricDishes){
            lowCaloricDishesName.add(d.getName());
        }
        return lowCaloricDishesName;
    }


}
