package com.slt.java8.java8InAction.stream.streamAPI.createStream;

import java.util.stream.Stream;

/**
 * 由值创建流
 */
public class CreateStream01 {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("Java 8 ", "Lambdas ", "In ", "Action");
        stream.map(String::toUpperCase).forEach(System.out::println);

        Stream.empty();
    }
}
