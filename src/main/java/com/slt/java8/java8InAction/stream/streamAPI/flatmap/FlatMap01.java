package com.slt.java8.java8InAction.stream.streamAPI.flatmap;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * 对于一张单词表，如何返回一张列表，列出里面各不相同的字符
 * ["Hello","World"]，返回列表["H","e","l", "o","W","r","d"]
 *
 * 错误版本 无法解决
 */
public class FlatMap01 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("Hello", "World","Hello");
        //word.split("") 返回 string[]  想要的是 Stream<String>来表示一个字符流
        List<String[]> collect = words.stream()
                .map(word -> word.split(""))
                .distinct()
                .collect(toList());

        for (String[] s : collect) {
            System.out.println(Arrays.toString(s));
        }
    }
}
