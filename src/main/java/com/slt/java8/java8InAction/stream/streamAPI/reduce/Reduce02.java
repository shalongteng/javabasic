package com.slt.java8.java8InAction.stream.streamAPI.reduce;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * 无初始值版本 reduce：
 * 使用初始值 和后边每一个数都去做同样操作 得到最终的值 返回
 */
public class Reduce02 {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

        /**
         * 无初始值版本 reduce：
         * 一个BinaryOperator<T>来将两个元素结合起来产生一个新值，这里我们用的是
         * lambda (a, b) -> a + b。
         */
        Optional<Integer> reduce = numbers.stream().reduce((a, b) -> a + b);
        int sum3 = numbers.stream().reduce(0, Integer::sum);


        System.out.println(reduce.get());
        System.out.println(sum3);
    }
}
