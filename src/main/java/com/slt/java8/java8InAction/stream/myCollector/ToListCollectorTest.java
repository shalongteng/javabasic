package com.slt.java8.java8InAction.stream.myCollector;

import com.slt.java8.java8InAction.stream.Dish;
import com.slt.java8.java8InAction.stream.Menu;

import java.util.*;
import java.util.stream.Stream;

public class ToListCollectorTest{

    public static void main(String[] args) {
        Stream<Dish> menuStream = Menu.getMenu().stream();
        List<Dish> dishes = menuStream
                .filter(Dish::isVegetarian)
                .collect(new ToListCollector<Dish>());
        System.out.println(dishes);

        List<Dish> dishes2 = Menu.getMenu().stream().collect(
                ArrayList::new,
                List::add,
                List::addAll);
        System.out.println(dishes2);
    }
}