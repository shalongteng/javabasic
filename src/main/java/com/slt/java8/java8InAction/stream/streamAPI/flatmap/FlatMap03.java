package com.slt.java8.java8InAction.stream.streamAPI.flatmap;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * 对于一张单词表，如何返回一张列表，列出里面各不相同的字符
 * ["Hello","World"]，返回列表["H","e","l", "o","W","r","d"]
 * <p>
 * 错误版本 无法解决
 */
public class FlatMap03 {
    public static void main(String[] args) {
        //你需要一个字符流，而不是数组流
        String[] arrayOfWords = {"Goodbye", "World"};
        Arrays.stream(arrayOfWords);

        List<String> words = Arrays.asList("Hello", "World","Hello");

        //将各个生成流扁平化为单个流
        //在map里返回的是stream流，这时候可以用flatmap 将所有stream流合并
        List<String> collect = words.stream()
                .map(word -> word.split(""))
                .flatMap(Arrays::stream)//将流合并
                .distinct()
                .collect(toList());

        System.out.println(collect);

    }
}
