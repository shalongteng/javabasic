package com.slt.java8.java8InAction.stream.exercise;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * 给定两个数字列表，如何返回所有的数对呢？例如，给定列表[1, 2, 3]和列表[3, 4]，应
 * 该返回[(1, 3), (1, 4), (2, 3), (2, 4), (3, 3), (3, 4)]。
 * 为简单起见，你可以用有两个元素的数组来代表数对。
 */
public class Exercise01 {
    public static void main(String[] args) {
        List<Integer> list1 = Arrays.asList(1, 2, 3);
        List<Integer> list2 = Arrays.asList(3, 4);

//        getAllCom2(list1, list2);
        getAllCom3(list1, list2);
    }

    //java8 之前写法
    private static void getAllCom(List<Integer> list1, List<Integer> list2) {
        for (int i = 0; i < list1.size(); i++) {
            for (int j = 0; j < list2.size(); j++) {
                System.out.println("[" + list1.get(i) + "," + list2.get(j) + "]");
            }
        }
    }

    //相当于两层 for循环
    private static void getAllCom2(List<Integer> list1, List<Integer> list2) {
        list1.stream()
                .forEach(i -> list2.stream().forEach(j -> System.out.println("[" + i + "," + j + "]")));

    }

    //flatmap 合并成一个流
    private static void getAllCom3(List<Integer> list1, List<Integer> list2) {
        List<int[]> collect = list1.stream()
                .flatMap(i -> list2.stream()//相当于 两层for循环
                        .filter(j -> (i + j) % 3 == 0)//返回总和能被3整除的数对
                        .map(j -> new int[]{i, j})//
                )
                .collect(toList());

        for (int[] ints : collect) {
            System.out.println(Arrays.toString(ints));
        }
    }

    /**
     * 在map里返回的是stream流，这时候可以用flatmap 将所有stream流合并
     */
    private static void getAllCom4(List<Integer> list1, List<Integer> list2) {
        List<Stream<int[]>> collect = list1.stream()
                .map(i -> list2.stream()//相当于 两层for循环
                        .filter(j -> (i + j) % 3 == 0)//返回总和能被3整除的数对
                        .map(j -> new int[]{i, j})//
                )
                .collect(toList());


    }


}
