package com.slt.java8.java8InAction.stream.streamAPI;

public enum Type {
    MEAT, FISH, OTHER
}