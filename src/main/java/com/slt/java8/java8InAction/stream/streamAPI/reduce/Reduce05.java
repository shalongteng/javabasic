package com.slt.java8.java8InAction.stream.streamAPI.reduce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * 怎样用map和reduce方法数一数流中有多少个菜呢?
 *
 * 用reduce方法来实现toList Collector所做的工作：
 */
public class Reduce05 {
    public static void main(String[] args) {
        Stream<Integer> stream = Arrays.asList(1, 2, 3, 4, 5, 6).stream();
        List<Integer> numbers = stream.reduce(
                new ArrayList<Integer>(),
                (List<Integer> l, Integer e) -> {
                    l.add(e);
                    return l; },
                (List<Integer> l1, List<Integer> l2) -> {
                    l1.addAll(l2);
                    return l1; });

        System.out.println(numbers);

    }
}
