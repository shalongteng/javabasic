package com.slt.java8.sourceCode;

import java.util.Arrays;
import java.util.List;

/**
 * foreach 有两个实现 head 和 ReferencePipeline
 */
public class SourceCodeTest {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 1, 4, 3, 2, 4);
        //源直接foreach
        list.stream().forEach(System.out::println);

        //有中间操作的 foreach
        list.stream()
                .map(item -> item)
                .forEach(System.out::println);
    }
}
