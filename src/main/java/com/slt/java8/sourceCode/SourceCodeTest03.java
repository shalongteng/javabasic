package com.slt.java8.sourceCode;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * stream 调用流程分析
 */
public class SourceCodeTest03 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4);
        /**
         * 构造流
         * StreamSupport.stream(spliterator(), false) 此处是ArraySpliterator
         * 使用 内部类head 构造流的源头
         * new ReferencePipeline.Head<>
         * 最后new AbstractPipeline 构造一个流管道 做为流的源头
         */
        Stream<Integer> stream1 = list.stream();
        /**
         * new StatelessOp 追加到流源头上一个流管道中间阶段
         *
         */
        Stream<Integer> stream2 = stream1.filter(item -> item > 1);
        System.out.println("11111111111");
        /**
         * 此时流管道已经连接了起来，形成了一个双向连表
         * source-> filter stage -> map stage
         */
        Stream<Integer> stream3 = stream2.map(item -> item + 1);
        System.out.println("222222222222");
        /**
         * 终端操作 forEach
         * foreach实现有两处 一处是head 一处是pipeline
         */
        stream3.forEach(System.out::println);
    }
}
