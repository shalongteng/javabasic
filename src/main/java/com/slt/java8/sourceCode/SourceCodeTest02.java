package com.slt.java8.sourceCode;

import java.util.Arrays;
import java.util.List;

/**
 * map
 * 只有一个实现类 ReferencePipeline
 * copyinto
 */
public class SourceCodeTest02 {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4);

        //map 不会触发操作
//        list.stream().map(item -> item);
        list.stream()
                .filter(item->item > 1)
                .map(item -> item + 1)
                .forEach(System.out::println);
    }
}
